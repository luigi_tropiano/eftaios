package it.polimi.ingsw.cg_6.server.model.map;

import java.awt.Point;

public class EscapeHatchSector extends Sector {

    private boolean available;

    public EscapeHatchSector(Point point) {
        setPoint(point);
        available = true;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }
}
