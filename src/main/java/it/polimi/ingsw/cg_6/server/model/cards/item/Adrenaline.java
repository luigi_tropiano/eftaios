package it.polimi.ingsw.cg_6.server.model.cards.item;

import it.polimi.ingsw.cg_6.server.model.GameState;
import it.polimi.ingsw.cg_6.server.model.move.TwoSectorsMove;

/**
 * the card Adrenaline
 * 
 * @author Daniele Parigi
 * @author Luigi Tropiano
 *
 */
public class Adrenaline extends ItemCard {

    @Override
    public String toString() {
        return "adrenaline";
    }

    /**
     * makes the human player move two sectors, updates the log, discards the
     * card and removes it from the player hand
     * @param gameState
     * @return The String to describe action performed
     */
    @Override
    public String activate(GameState gameState) {
        gameState.getCurrentPlayer().setMove(new TwoSectorsMove());
        gameState.addLog(gameState.getCurrentPlayer().toString()
                + " uses item [" + this.toString() + "]");
        gameState.getItemDeck().discardCard(this);
        gameState.getCurrentPlayer().getItems().remove(this);
        return "you feel a renewed sense of energy";
    }
}
