package it.polimi.ingsw.cg_6.server.model.cards.sector;

import it.polimi.ingsw.cg_6.server.model.GameState;
import it.polimi.ingsw.cg_6.server.model.character.Player;
/**
 * The silence card
 * @author Daniele Parigi
 * @author Luigi Tropiano
 */
public class Silence extends SectorCard {
    /**
     * activate of the silence card
     * @param player
     * @param gameState
     * @return "you drew a silence"
     */
    @Override
    public String activate(Player player, GameState gameState) {
        gameState.addLog(player.toString() + " draws a silence");
        return "you drew a silence";
    }
}
