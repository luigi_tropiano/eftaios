package it.polimi.ingsw.cg_6.server;

import it.polimi.ingsw.cg_6.client.rmi.SubscriberInterface;
import it.polimi.ingsw.cg_6.server.socket.BrokerThread;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Topic {

    private Map<UUID, Object> subscribers = new HashMap<UUID, Object>();
    private static Logger logger = Logger.getLogger("escape");

    public Map<UUID, Object> getSubscribers() {
        return subscribers;
    }

    public String publish(String msg) {
        dispatch(msg);
        return "message sent";
    }

    public void subscribe(UUID id, Object obj) {
        subscribers.put(id, obj);
    }

    public void unsubscribe(UUID id) {
        subscribers.remove(id);
    }

    public void sendTo(UUID id, String msg) {
        if (subscribers.get(id) instanceof BrokerThread)
            ((BrokerThread) subscribers.get(id)).dispatchMessage(msg);
        else if (subscribers.get(id) instanceof SubscriberInterface)
            try {
                ((SubscriberInterface) subscribers.get(id))
                        .dispatchMessage(msg);
            } catch (RemoteException e) {
                logger.log(Level.WARNING, "server problem", e);
            }
    }

    /**
     * 2) Questo metodo manda un messaggio a tutti i subscriber presenti nella
     * lista. Di fatto la publish si riduce ad iterare sulla lista subscriber ed
     * a chiamare un metodo interno a ciascun subscriber, il quale a sua volta
     * gestirá l'invio del messaggio.
     * 
     * @param msg
     *            Il messaggio da mandare.
     */

    private void dispatch(String msg) {
        if (!subscribers.isEmpty()) {
            System.out.println("Publishing message");
            for (Object sub : subscribers.values()) {
                if (sub instanceof BrokerThread) {
                    ((BrokerThread) sub).dispatchMessage(msg);
                } else if (sub instanceof SubscriberInterface)
                    try {
                        ((SubscriberInterface) sub).dispatchMessage(msg);
                    } catch (RemoteException e) {
                        logger.log(Level.WARNING, "server problem", e);
                    }
            }
        } else {
            System.err.println("No socket subscribers!!");
        }
    }

}