package it.polimi.ingsw.cg_6.server.model.character;

import it.polimi.ingsw.cg_6.server.model.map.GameMap;
import it.polimi.ingsw.cg_6.server.model.move.TwoSectorsMove;

import java.util.UUID;
/**
 * the character Alien
 * @author Daniele Parigi
 * @author Luigi Tropiano
 */
public class Alien extends Player {
    
    public Alien(UUID clientId) {
        super(clientId);
        setMove(new TwoSectorsMove());
        this.setCanAttack(true);
    }

    @Override
    public void setInitialPosition(GameMap gameMap) {
        setPosition(gameMap.getAlienSector());
    }

    @Override
    public String getRole() {
        return "alien";
    }
}
