package it.polimi.ingsw.cg_6.server.socket;

import it.polimi.ingsw.cg_6.server.controller.ServerTable;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Broker extends Thread {
    private final int portNumber = 28999;
    private boolean listening = true;
    private ServerTable serverTable;
    public static final Logger logger = Logger.getLogger("escape");

    public Broker(ServerTable serverTable) {
        this.serverTable = serverTable;
    }

    /**
     * 1) Vogliamo creare un servizio Broker che rimanga in ascolto per
     * eventuali sottoscrizioni. Quindi creaiamo una ServerSocket e, per
     * accettare sottoscrizioni multiple, creiamo un loop. Quindi ci mettiamo in
     * attesa che un client esterno si sottoscriva (brokerSocket.accept, The
     * method blocks until a connection is made. A new Socket is created);
     * quando ció avviene viene creato un nuovo thread, brokerThread, che
     * rappresenterá la specifica connessione allo specifico client/subscriber.
     * Il brokerThread viene avviato e salvato nella lista che raggruppa i
     * subscribers. Quindi la ServerSocket torna in ascolto di altre
     * connessioni.
     */
    @Override
    public void run() {
        try (ServerSocket brokerSocket = new ServerSocket(portNumber)) {
            while (listening) {
                BrokerThread brokerThread = new BrokerThread(
                        brokerSocket.accept(), serverTable);
                brokerThread.start();
            }
        } catch (IOException e) {
            logger.log(Level.SEVERE, "run on broker", e);
        }
    }
}