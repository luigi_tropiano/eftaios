package it.polimi.ingsw.cg_6.server.model.cards.hatch;

import it.polimi.ingsw.cg_6.client.InputConverter;
import it.polimi.ingsw.cg_6.server.model.GameState;
import it.polimi.ingsw.cg_6.server.model.character.Player;
import it.polimi.ingsw.cg_6.server.model.map.EscapeHatchSector;
/**
 * the card hatch open
 * @author Daniele Parigi
 * @author Luigi Tropiano
 */
public class HatchOpen extends HatchCard {
    /**
     * activate the card and updates log
     * @param player that draws this
     * @param gameState
     * @return "you entered the escape hatch and fly away"
     */
    @Override
    public String activate(Player player, GameState gameState) {
        player.setWinner(true);
        ((EscapeHatchSector) player.getPosition()).setAvailable(false);
        gameState.addLog(player.toString()
                + " got to the escape hatch in sector ["
                + InputConverter.intToChar(player.getPosition().getPoint().x)
                + " " + player.getPosition().getPoint().y
                + " and found it open. He left the spaceship alive");
        gameState.getPlayerQueue().removePlayer(player);
        return "you entered the escape hatch and fly away";
    }
}
