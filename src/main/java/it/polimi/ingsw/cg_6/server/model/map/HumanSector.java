package it.polimi.ingsw.cg_6.server.model.map;

import java.awt.Point;

public class HumanSector extends Sector {

    public HumanSector(Point point) {
        setPoint(point);
    }
}
