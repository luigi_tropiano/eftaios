package it.polimi.ingsw.cg_6.client.CLI;

import java.util.Scanner;

import sun.net.util.IPAddressUtil;

@SuppressWarnings("restriction")
public class CLI {
    
    private static String IP;
    
    public static void main(String[] args) {
        while (true) {
            System.out.println("Socket or RMI?");
            Scanner stdin = new Scanner(System.in);
            String answer = stdin.nextLine();
            if ("rmi".equalsIgnoreCase(answer)) {
                CLIRMIView.start();
                break;
            }
            if ("socket".equalsIgnoreCase(answer)) {
                CLISocketView.start();
                break;
            }
        }   
    }
    
    public static String waitForIP() {
        while (true) {
            System.out.println("HOST IP address:");
            Scanner stdin = new Scanner(System.in);
            IP = stdin.nextLine();
            if (IPAddressUtil.isIPv4LiteralAddress(IP))
                return IP;
            if (IP.equals("localhost"))
                return IP;
        }
    }
    // private static int waitForPort() {
    // System.out.println("Port:");
    // Scanner stdin = new Scanner(System.in);
    // int PORT = Integer.parseInt(stdin.nextLine());
    // return PORT;
    // }
}
