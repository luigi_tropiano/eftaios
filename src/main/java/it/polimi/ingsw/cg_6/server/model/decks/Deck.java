package it.polimi.ingsw.cg_6.server.model.decks;

import it.polimi.ingsw.cg_6.server.model.cards.Card;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class Deck {
    private List<Card> discardedCards;
    private List<Card> cards;

    public Deck() {
        cards = new ArrayList<Card>();
        discardedCards = new ArrayList<Card>();
    }

    public List<Card> getDiscardedCards() {
        return discardedCards;
    }

    public List<Card> getCards() {
        return cards;
    }

    /**
     * Pesca una carta e la ritorna. Se e' l'ultima carta del mazzo resetta il
     * mazzo.
     * 
     * @return the drawn card
     */
    public Card drawCard() {
        Card firstCard = cards.remove(0);
        if (cards.isEmpty())
            resetDeck();
        return firstCard;
    }

    protected void addCard(Card card) {
        this.cards.add(card);
    }

    /**
     * Aggiunge la carta che gli viene passata all'array delle carte scartate.
     * 
     * @param Card
     */
    public void discardCard(Card card) {
        this.discardedCards.add(card);
    }

    protected void shuffleDeck() {
        Collections.shuffle(cards);
    }

    /**
     * Cambia il puntatore al mazzo con quello delle carte scartate. Crea un
     * nuovo array (vuoto) delle carte scartate. Mischia il nuovo mazzo.
     */
    public void resetDeck() {
        cards = discardedCards;
        discardedCards = new ArrayList<Card>();
        shuffleDeck();
    }

}
