package it.polimi.ingsw.cg_6.server.model.move;

import it.polimi.ingsw.cg_6.server.model.character.Alien;
import it.polimi.ingsw.cg_6.server.model.character.Player;
import it.polimi.ingsw.cg_6.server.model.map.EscapeHatchSector;
import it.polimi.ingsw.cg_6.server.model.map.Sector;

public class OneSectorMove implements Move {

    @Override
    public boolean isValid(Player player, Sector arrival) {
        Sector start = player.getPosition();
        if ((arrival instanceof EscapeHatchSector) && !((EscapeHatchSector) arrival).isAvailable())
            return false;
        if ((player instanceof Alien) && (arrival instanceof EscapeHatchSector))
            return false;
        if (start.getNearSectors().contains(arrival))
            return true;
        else
            return false;
    }

}
