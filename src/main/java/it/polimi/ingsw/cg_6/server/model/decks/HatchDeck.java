package it.polimi.ingsw.cg_6.server.model.decks;

import it.polimi.ingsw.cg_6.server.model.cards.hatch.HatchClosed;
import it.polimi.ingsw.cg_6.server.model.cards.hatch.HatchOpen;

public class HatchDeck extends Deck {

    public HatchDeck() {
        for (int i = 3; i > 0; i--) {
            addCard(new HatchClosed());
            addCard(new HatchOpen());

            shuffleDeck();
        }
    }
}
