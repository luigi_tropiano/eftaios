package it.polimi.ingsw.cg_6.server.model.character;

import it.polimi.ingsw.cg_6.server.model.map.GameMap;
import it.polimi.ingsw.cg_6.server.model.move.OneSectorMove;

import java.util.UUID;

/**
 * the character Human
 * 
 * @author Daniele Parigi
 * @author Luigi Tropiano
 */
public class Human extends Player {

    public Human(UUID clientId) {
        super(clientId);
        setMove(new OneSectorMove());
    }

    @Override
    public void setInitialPosition(GameMap gameMap) {
        setPosition(gameMap.getHumanSector());
    }

    /**
     * reset the flag of the human player and reset the move if it was updated
     * by an Adrenaline Card
     */
    @Override
    public void resetFlags() {
        this.setDidAttack(false);
        this.setDidDraw(false);
        this.setDidMove(false);
        this.setMove(new OneSectorMove());
        this.setCanAttack(false);
    }

    @Override
    public String getRole() {
        return "human";
    }
}
