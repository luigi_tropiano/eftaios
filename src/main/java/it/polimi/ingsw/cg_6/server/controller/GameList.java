package it.polimi.ingsw.cg_6.server.controller;

import it.polimi.ingsw.cg_6.server.model.GameState;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * collect the game started in the server
 * 
 * @author Daniele Parigi
 * @author Luigi Tropiano
 *
 */
public class GameList {

    private Map<Integer, GameState> games;
    private Integer gameKey = 1;

    public GameList() {
        this.games = new ConcurrentHashMap<Integer, GameState>();
    }

    public Map<Integer, GameState> getGames() {
        return games;
    }

    /**
     * add a Game
     * 
     * @param game
     *            to add
     * @return the game ID
     */
    public Integer addGame(GameState game) {
        games.put(gameKey, game);
        game.setGameId(gameKey);
        Integer tmp = gameKey;
        gameKey++;
        return tmp;
    }

    public void deleteGame(Integer GameId) {
        games.remove(GameId);
    }
}
