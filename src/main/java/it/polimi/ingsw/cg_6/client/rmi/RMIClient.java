package it.polimi.ingsw.cg_6.client.rmi;

import it.polimi.ingsw.cg_6.client.InputConverter;
import it.polimi.ingsw.cg_6.server.rmi.ActionInterface;
import it.polimi.ingsw.cg_6.server.rmi.BrokerInterface;

import java.awt.Point;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Queue;
import java.util.Scanner;
import java.util.UUID;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * RMI client
 * 
 * @author Daniele Parigi
 * @author Luigi Tropiano
 */
public class RMIClient implements SubscriberInterface, Runnable {
    private static String HOST;
    private static int PORT;
    private static final String NAME = "server";
    private final UUID id = UUID.randomUUID();
    private Logger logger = Logger.getLogger("escape");
    private ActionInterface actions;
    private BrokerInterface broker;
    private SubscriberInterface subscriberInterface;
    private Queue<String> buffer = new ConcurrentLinkedDeque<String>();

    public RMIClient(String HOST, int PORT) {
        RMIClient.HOST = HOST;
        RMIClient.PORT = PORT;
    }

    /**
     * request a join to access room and Topic and start to scanner the command
     * from the user
     *
     */
    @Override
    public void run() {
        init();
        try {
            // set registry
            Registry registry = LocateRegistry.getRegistry(HOST, PORT);
            actions = (ActionInterface) registry.lookup(NAME);
            dispatchMessage("joining room...");
            // join room
            actions.join(id);
            Registry brokerRegistry = LocateRegistry.getRegistry(HOST, 1098);
            // subscribe to topic
            broker = (BrokerInterface) brokerRegistry.lookup("broker");
            broker.subscribe(id, subscriberInterface);
            // Start to scanner for users input
            Scanner stdin = new Scanner(System.in);
            while (true) {
                // read the string from the standard input
                String inputLine = stdin.nextLine();
                // pars the input
                parse(actions, inputLine);
            }
        } catch (RemoteException | NotBoundException e) {
            logger.log(Level.SEVERE, "run", e);
        }
    }

    private void init() {

        try {
            // export the SubscriberInterface
            // required for dispatchMessage() remote invocation
            subscriberInterface = ((SubscriberInterface) UnicastRemoteObject
                    .exportObject(this, 0));
        } catch (RemoteException e) {
            logger.log(Level.SEVERE, "init throws", e);
        }

    }

    public UUID getId() {
        return id;
    }

    public ActionInterface getActions() {
        return actions;
    }

    /**
     * parser of the users input
     * 
     * @param actions
     *            the actions interface
     * @param line
     *            to pars
     */
    private void parse(ActionInterface actions, String line) {
        String[] split;
        split = line.split(" ");
        try {
            switch (split[0]) {
            case "chat":
                print(actions.chat(id, line));
                break;
            case "move":
                if (catcher(split, 3, "move")) {
                    split[1] = Integer.toString(InputConverter
                            .charToInt(split[1].charAt(0)));
                    print(actions.move(
                            id,
                            new Point(Integer.parseInt(split[1]), Integer
                                    .parseInt(split[2]))));
                }
                break;
            case "draw":
                print(actions.draw(id));
                break;
            case "attack":
                print(actions.attack(id));
                break;
            case "noise":
                if (catcher(split, 3, "noise")) {
                    split[1] = Integer.toString(InputConverter
                            .charToInt(split[1].charAt(0)));
                    print(actions.noise(
                            id,
                            new Point(Integer.parseInt(split[1]), Integer
                                    .parseInt(split[2]))));
                }
                break;
            case "use":
                if (split.length < 2) {
                    System.out.println("usage: use <item>");
                    break;
                }
                if (split.length > 1 && "spotlight".equalsIgnoreCase(split[1])
                        && catcher(split, 4, "use spotlight")) {
                    print(actions.use(id, line));
                }
                print(actions.use(id, line));
                break;
            case "end":
                print(actions.endTurn(id));
                break;
            case "discard":
                if (split.length != 2) {
                    System.out.println("usage: discard <item>");
                    break;
                }
                print(actions.discardItem(id, line));
                break;
            default:
                System.out.println("command refused");
            }
        } catch (RemoteException e) {
            logger.log(Level.SEVERE, "parser problem", e);
        }
    }

    private void print(String line) {
        System.out.println(line);
    }

    /**
     * check the format of the string
     * 
     * @param split
     *            to check
     * @param size
     *            max size of the right string
     * @param name
     *            of the method for return propose
     * @return true if the String passed matches the controls
     */
    private boolean catcher(String[] split, int size, String name) {
        if (split.length != size || !split[size - 2].matches("\\p{Alpha}")
                || !split[size - 1].matches("\\d(\\d)?")) {
            System.out.println("usage: " + name + " <letter> <number>");
            return false;
        }
        return true;
    }

    /**
     * @param msg
     *            is the message sent by the broker by invoking subscriber's
     *            remote interface the method simply prints the message received
     *            by the broker
     */
    @Override
    public void dispatchMessage(String msg) {
        buffer.add(msg);
        System.out.println(msg);
        synchronized (buffer) {
            buffer.notify();
        }
    }

    public Queue<String> getBuffer() {
        return buffer;
    }
}
