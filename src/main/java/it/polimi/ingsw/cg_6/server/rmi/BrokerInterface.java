package it.polimi.ingsw.cg_6.server.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.UUID;

public interface BrokerInterface extends Remote {

    public void subscribe(UUID id, Object obj) throws RemoteException;

}
