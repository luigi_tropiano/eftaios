package it.polimi.ingsw.cg_6.server.model.move;

import it.polimi.ingsw.cg_6.server.model.character.Player;
import it.polimi.ingsw.cg_6.server.model.map.Sector;

public interface Move {
    public boolean isValid(Player player, Sector arrival);
}
