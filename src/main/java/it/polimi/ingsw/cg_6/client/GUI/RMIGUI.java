package it.polimi.ingsw.cg_6.client.GUI;

import it.polimi.ingsw.cg_6.client.rmi.RMIClient;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.rmi.RemoteException;
import java.util.logging.Level;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.text.DefaultCaret;

/**********************************
 * This is the main class of a Java program to play a game based on hexagonal
 * tiles. The mechanism of handling hexes is in the file Drawer.java.
 * 
 * Adapted from a class written by: M.H. on Date: December 2012
 * 
 * @author Daniele Parigi
 * @author Luigi Tropiano
 ***********************************/

public class RMIGUI extends GUI implements Runnable {

    private RMIClient client;
    private DrawingPanel panel;

    public DrawingPanel getPanel() {
        return panel;
    }

    public RMIGUI(RMIClient client) {
        this.client = client;
    }

    @Override
    public void run() {
        initGame();
        createAndShowGUI();
    }

    /**
     * creates the GUI to play the game, the GUI has a cliccable game map, a
     * chat panel for chatting and receiving server messages and a Panel with
     * input buttons
     */
    private void createAndShowGUI() {

        // creating the skeleton panels
        panel = new DrawingPanel();
        JPanel rightPanel = new JPanel();
        JPanel buttons = new JPanel();
        JPanel Upbutton = new JPanel();
        JPanel downbutton = new JPanel();

        // creating the main frame
        JFrame frame = new JFrame("EFTAIOS");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new BorderLayout());

        // creating buttons' panels
        buttons.setLayout(new BorderLayout());
        buttons.add(Upbutton, BorderLayout.NORTH);
        buttons.add(downbutton, BorderLayout.SOUTH);

        // creating item buttons
        Upbutton.setLayout(new FlowLayout());
        Upbutton.add(new AlphaContainer(itemButtonSetup("adrenaline")));
        Upbutton.add(new AlphaContainer(itemButtonSetup("sedatives")));
        Upbutton.add(new AlphaContainer(itemButtonSetup("teleport")));
        Upbutton.add(new AlphaContainer(itemButtonSetup("attack")));
        JButton spotlight = buttonSetup("spotlight");
        Upbutton.add(new AlphaContainer(spotlight));
        spotlight.addActionListener(new SpotlightButtonListener());

        // creating attack, draw, endturn, discard buttons
        JButton attack, draw, endturn, discard;
        downbutton.setLayout(new FlowLayout());
        discard = buttonSetup("discard");
        downbutton.add(new AlphaContainer(discard));
        discard.addActionListener(new DiscardButtonListener());
        attack = buttonSetup("attack1");
        downbutton.add(new AlphaContainer(attack));
        attack.addActionListener(new AttackButtonListener());
        draw = buttonSetup("draw");
        downbutton.add(new AlphaContainer(draw));
        draw.addActionListener(new DrawButtonListener());
        endturn = buttonSetup("endturn");
        downbutton.add(new AlphaContainer(endturn));
        endturn.addActionListener(new EndturnButtonListener());
        // right panel setting
        rightPanel.setLayout(new BorderLayout());
        rightPanel.add(createChatPanel(), BorderLayout.NORTH);
        rightPanel.add(buttons, BorderLayout.CENTER);

        panel.setBackground(new Color(0, 0, 0, 0));
        // adding panel to frame
        frame.add(new AlphaContainer(panel), BorderLayout.CENTER);
        frame.add(rightPanel, BorderLayout.EAST);
        // finalizing frame
        frame.setSize(SCRWIDTH, SCRHEIGHT);
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        panel.repaint();
    }

    /**
     * setup a item button whit icon and action listener
     * 
     * @param line
     *            the name of the button icon
     * @return the JButton created
     */
    public JButton itemButtonSetup(String line) {
        Border emptyBorder = BorderFactory.createEmptyBorder();
        Icon icon = new ImageIcon("res" + File.separatorChar + line + ".png");
        JButton button = new JButton(icon);
        button.setBorder(emptyBorder);
        button.setBackground(COLOURBACK);
        button.setFocusPainted(false);
        button.addActionListener(new ActionItemButtonListener(line));
        return button;
    }

    private JButton sendMessage;

    /**
     * create a simple chat panel, at the end start the pub-sub logic
     * 
     * @return the chat panel just created
     */
    public JPanel createChatPanel() {
        JPanel backPanel = new JPanel();
        JPanel northPanel = new JPanel();
        JPanel southPanel = new JPanel();
        JTextArea chatBox = new JTextArea(32, 20);
        JScrollPane scrollChat = new JScrollPane(new AlphaContainer(chatBox));
        messageBox = new JTextField();
        sendMessage = new JButton("Send");
        sendMessage.addActionListener(new sendMessageButtonListener());

        // jtextArea setting
        chatBox.setEditable(false);
        chatBox.setFont(new Font("Serif", Font.PLAIN, 15));
        chatBox.setLineWrap(true);
        chatBox.setBackground(new Color(0, 0, 0, 30));
        chatBox.setForeground(Color.BLACK);
        DefaultCaret caret = (DefaultCaret) chatBox.getCaret();
        caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
        // putting chatBox in northPanel
        northPanel.setLayout(new BorderLayout());
        northPanel.setOpaque(false);
        // jtextField setting
        messageBox.requestFocusInWindow();
        messageBox.setBorder(BorderFactory.createLineBorder(Color.WHITE, 1));
        messageBox.setOpaque(false);
        messageBox.setForeground(Color.BLACK);
        // southPanel setting
        southPanel.setLayout(new GridBagLayout());
        southPanel.setBackground(new Color(0, 0, 0, 70));
        southPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        GridBagConstraints left = new GridBagConstraints();
        left.anchor = GridBagConstraints.LINE_START;
        left.fill = GridBagConstraints.HORIZONTAL;
        left.weightx = 512.0D;
        left.weighty = 1.0D;

        GridBagConstraints right = new GridBagConstraints();
        right.insets = new Insets(0, 10, 0, 0);
        right.anchor = GridBagConstraints.LINE_END;
        right.fill = GridBagConstraints.NONE;
        right.weightx = 1.0D;
        right.weighty = 1.0D;

        northPanel.add(scrollChat, BorderLayout.CENTER);

        southPanel.add(messageBox, left);
        southPanel.add(sendMessage, right);

        scrollChat.setOpaque(false);
        scrollChat.getViewport().setOpaque(false);
        scrollChat.setBorder(null);

        backPanel.setBackground(Color.WHITE);
        backPanel.setLayout(new BorderLayout());
        backPanel.add(northPanel, BorderLayout.CENTER);
        backPanel.add(new AlphaContainer(southPanel), BorderLayout.SOUTH);
        // making the textbox capable of receiving server messages
        RMIBuffer buffer = new RMIBuffer(client, chatBox, this);
        new Thread(buffer).start();
        return backPanel;
    }

    /**
     * Button listener for send the chat
     *
     */
    class sendMessageButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (messageBox.getText().length() < 1) {
                // do nothing
            } else if (".clear".equals(messageBox.getText())) {
                chatBox.setText("Cleared all messages\n");
                messageBox.setText("");
            } else {
                try {
                    // BANANE is random word for CLI-GUI compatibility
                    client.getActions().chat(
                            client.getId(),
                            "1234567890123456789012345678901234567890 "
                                    + messageBox.getText() + "\n");
                } catch (RemoteException e) {
                    logger.log(Level.SEVERE, "Chat not working", e);
                }
                messageBox.setText("");
            }
            messageBox.requestFocusInWindow();
        }
    }

    /**
     * action listener of all the item button
     *
     */
    class ActionItemButtonListener implements ActionListener {

        private String line;

        public ActionItemButtonListener(String line) {
            this.line = "use " + line;
        }

        @Override
        public void actionPerformed(ActionEvent event) {
            try {
                client.dispatchMessage(client.getActions().use(client.getId(),
                        line));
            } catch (RemoteException e) {
                logger.log(Level.WARNING, "remote problem", e);
            }
        }
    }

    /**
     * button listener for the spotlight: simply set a boolean to advise the
     * mouse listener of the map to send a spotlight use
     *
     */
    class SpotlightButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent event) {
            setSpotlight(true);
        }
    }

    /**
     * button listener for discard, create a frame to choose which item to
     * discard
     *
     */
    class DiscardButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (!isBottunDisabler()) {
                setBottunDisabler(true);
                JFrame popup = new JFrame("Discard");
                popup.setSize(200, 110);
                popup.setUndecorated(true);
                popup.setLocationRelativeTo(null);
                popup.setResizable(false);
                popup.setVisible(true);
                popup.setLayout(new FlowLayout());
                popup.add(new AlphaContainer(discardButtonSetup("adrenaline",
                        popup)));
                popup.add(new AlphaContainer(discardButtonSetup("sedatives",
                        popup)));
                popup.add(new AlphaContainer(discardButtonSetup("teleport",
                        popup)));
                popup.add(new AlphaContainer(
                        discardButtonSetup("attack", popup)));
                popup.add(new AlphaContainer(discardButtonSetup("spotlight",
                        popup)));
                popup.add(new AlphaContainer(discardButtonSetup("defense",
                        popup)));
            }
        }
    }

    /**
     * creates one button for a popup frame
     * 
     * @param line
     *            name of the icon to pick
     * @param popup
     * @return the button just created
     */
    public JButton discardButtonSetup(String line, JFrame popup) {
        Border emptyBorder = BorderFactory.createEmptyBorder();
        Icon icon = new ImageIcon("res" + File.separatorChar + line + ".png");
        JButton button = new JButton(icon);
        button.setBorder(emptyBorder);
        button.setBackground(COLOURBACK);
        button.setFocusPainted(false);
        button.addActionListener(new DiscardItemButtonListener(line, popup));
        return button;
    }

    /**
     * the listener of discard item button
     *
     */
    class DiscardItemButtonListener implements ActionListener {
        private String line;
        private JFrame popup;

        public DiscardItemButtonListener(String line, JFrame popup) {
            this.line = "discard " + line;
            this.popup = popup;
        }

        @Override
        public void actionPerformed(ActionEvent event) {
            try {
                client.dispatchMessage(client.getActions().discardItem(
                        client.getId(), line));
            } catch (RemoteException e) {
                logger.log(Level.WARNING, "remote problem", e);
            }
            popup.dispose();
            setBottunDisabler(false);
        }
    }

    /**
     * the listener of the attack button
     *
     */
    class AttackButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent event) {
            try {
                client.dispatchMessage(client.getActions().attack(
                        client.getId()));
            } catch (RemoteException e) {
                logger.log(Level.WARNING, "remote problem", e);
            }
        }
    }

    /**
     * the listener of the draw button
     *
     */
    class DrawButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent event) {
            try {
                client.dispatchMessage(client.getActions().draw(client.getId()));
            } catch (RemoteException e) {
                logger.log(Level.WARNING, "remote problem", e);
            }
        }
    }

    /**
     * the listener of the end turn button
     *
     */
    class EndturnButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent event) {
            try {
                client.dispatchMessage(client.getActions().endTurn(
                        client.getId()));
            } catch (RemoteException e) {
                logger.log(Level.WARNING, "remote problem", e);
            }
        }
    }

    /**
     * draw the hexagonal grid and fill it
     *
     */
    class DrawingPanel extends JPanel {

        private static final long serialVersionUID = 1L;

        public DrawingPanel() {

            MyMouseListener ml = new MyMouseListener();
            addMouseListener(ml);
            repaint();
        }

        @Override
        public void paintComponent(Graphics g) {
            Graphics2D g2 = (Graphics2D) g;
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                    RenderingHints.VALUE_ANTIALIAS_ON);
            g.setFont(new Font("TimesRoman", Font.PLAIN, 11));
            super.paintComponent(g2);
            // draw grid
            for (int i = 0; i < BSIZEX; i++) {
                for (int j = 0; j < BSIZEY; j++) {
                    Drawer.drawHex(i, j, g2);
                }
            }
            // fill in hexes
            for (int i = 0; i < BSIZEX; i++) {
                for (int j = 0; j < BSIZEY; j++) {
                    Drawer.fillHex(i, j, board[i][j], g2);
                }
            }
        }

        class MyMouseListener extends MouseAdapter { // inner class inside

            public MyMouseListener() {
            }

            // DrawingPanel
            public void mouseClicked(MouseEvent e) {
                Point p = new Point(Drawer.pxtoHex(e.getX(), e.getY()));
                if (p.x < 0 || p.y < 0 || p.x >= BSIZEX || p.y >= BSIZEY)
                    return;

                // setting what to do when a hexagon is clicked
                if (board[p.x][p.y] != EMPTY) {
                    repaint();
                    try {
                        if (isNoise()) {
                            client.dispatchMessage(client.getActions()
                                    .noise(client.getId(),
                                            new Point(p.x + 1, p.y + 1)));
                            setNoise(false);
                        } else if (isSpotlight()) {
                            int a = p.x + 1;
                            int b = p.y + 1;
                            client.dispatchMessage(client.getActions().use(
                                    client.getId(),
                                    "use spotlight " + a + " " + b));
                            setSpotlight(false);
                        } else
                            client.dispatchMessage(client.getActions()
                                    .move(client.getId(),
                                            new Point(p.x + 1, p.y + 1)));
                    } catch (RemoteException a) {
                        logger.log(Level.SEVERE, "Mouse Listener", a);
                    }
                }
            }
        } // end of MyMouseListener class
    } // end of DrawingPanel class
}
