package it.polimi.ingsw.cg_6.server.model.character;

public enum NickName {
	RED, BLUE, YELLOW, GREEN, PURPLE, BLACK, ORANGE, BROWN
}
