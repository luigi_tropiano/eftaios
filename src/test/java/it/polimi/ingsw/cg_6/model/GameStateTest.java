package it.polimi.ingsw.cg_6.model;

import static org.junit.Assert.*;
import it.polimi.ingsw.cg_6.server.Topic;
import it.polimi.ingsw.cg_6.server.model.GameState;
import it.polimi.ingsw.cg_6.server.model.cards.hatch.HatchCard;
import it.polimi.ingsw.cg_6.server.model.cards.hatch.HatchClosed;
import it.polimi.ingsw.cg_6.server.model.cards.hatch.HatchOpen;
import it.polimi.ingsw.cg_6.server.model.character.Alien;
import it.polimi.ingsw.cg_6.server.model.character.Human;
import it.polimi.ingsw.cg_6.server.model.character.Player;
import it.polimi.ingsw.cg_6.server.model.character.PlayersQueue;
import it.polimi.ingsw.cg_6.server.model.map.EscapeHatchSector;
import it.polimi.ingsw.cg_6.server.model.map.GameMap;
import it.polimi.ingsw.cg_6.server.model.map.MapParser;

import java.awt.Point;
import java.io.IOException;
import java.util.UUID;

import javax.xml.parsers.ParserConfigurationException;

import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

public class GameStateTest {

    private Player player1, player2, player3, player4;
    private Topic topic;
    private GameState gameState;
    private MapParser mp;
    private GameMap gameMap;
    private PlayersQueue playersQueue;
    private int turnLength;
    private HatchCard hatchOpen;
    private HatchClosed hatchClosed;

    @Before
    public void init() throws IOException, SAXException,
            ParserConfigurationException {
        turnLength = 180;
        player1 = new Alien(UUID.randomUUID());
        player2 = new Alien(UUID.randomUUID());
        player3 = new Human(UUID.randomUUID());
        player4 = new Human(UUID.randomUUID());
        mp = new MapParser();
        mp.createMap("Galilei");
        gameMap = mp.getMap();
        topic = new Topic();
        playersQueue = new PlayersQueue(topic);
        hatchOpen = new HatchOpen();
        hatchClosed = new HatchClosed();

        playersQueue.addPlayer(player1);
        playersQueue.addPlayer(player2);
        playersQueue.addPlayer(player3);
        playersQueue.addPlayer(player4);
        gameState = new GameState(gameMap, playersQueue, turnLength);
    }

    // 4 Giocatori, 2 Alieni e 1 umano ancora in gioco
    // L'umano attiva una carta Scialuppa e scappa
    @Test
    public void endGameOne() {
        playersQueue.removePlayer(player3);
        player4.setPosition(new EscapeHatchSector(new Point(2, 2)));
        hatchOpen.activate(player4, gameState);
        gameState.endTurn();
        assertFalse(player1.isWinner());
        assertFalse(player2.isWinner());
        assertTrue(gameState.isGameOver());
        assertTrue(player4.isWinner());
        assertEquals(0, playersQueue.getPlayers().size());
        assertEquals(4, playersQueue.getRemovedPlayers().size());
        assertFalse(((EscapeHatchSector) player4.getPosition()).isAvailable());
    }
    
    @Test
    public void endGameTwo() {
        playersQueue.removePlayer(player3);
        player4.kill(gameState);
        playersQueue.removePlayer(player1);
        gameState.endTurn();
        assertTrue(player1.isWinner());
        assertTrue(player2.isWinner());
        assertFalse(player3.isWinner());
        assertFalse(player4.isWinner());
        assertEquals(0, playersQueue.getPlayers().size());
        assertEquals(4, playersQueue.getRemovedPlayers().size());
    }

    @Test
    public void endGameThree() {
        gameState.setTurnsCounter(39);
        gameState.setCurrentPlayer(playersQueue.getPlayers().get(3));
        assertEquals(playersQueue.nextPlayer(gameState.getCurrentPlayer()), playersQueue.getPlayers().get(0));
        gameState.endTurn();
        assertEquals(40, gameState.getTurnsCounter());
        gameState.endTurn();
        assertTrue(player1.isWinner());
        assertTrue(player2.isWinner());
        assertFalse(player3.isWinner());
        assertFalse(player4.isWinner());
        assertEquals(0, playersQueue.getPlayers().size());
        assertEquals(4, playersQueue.getRemovedPlayers().size());
    }

    @Test
    public void endGameFour() {
        player3.setPosition(gameMap.getSector(new Point(2,2)));
        player4.setPosition(gameMap.getSector(new Point(2,13)));
        ((EscapeHatchSector)(gameMap.getSector(new Point(22,2)))).setAvailable(false);;
        ((EscapeHatchSector)(gameMap.getSector(new Point(22,13)))).setAvailable(false);;
        hatchOpen.activate(player3, gameState);
        hatchClosed.activate(player4, gameState);
        gameState.endTurn();
        assertTrue(player1.isWinner());
        assertTrue(player2.isWinner());
        assertTrue(player3.isWinner());
        assertFalse(player4.isWinner());
        assertEquals(0, playersQueue.getPlayers().size());
        assertEquals(4, playersQueue.getRemovedPlayers().size());
    }
}
