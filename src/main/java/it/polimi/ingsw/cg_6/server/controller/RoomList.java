package it.polimi.ingsw.cg_6.server.controller;

import it.polimi.ingsw.cg_6.server.Topic;

import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * This class handles the Waiting Rooms and let Clients to join them. It saves
 * into the ServerTable the Room where players are located. There is only a
 * single one WaitingRoom to join; when it is full or countdown ends, a new one is created and the
 * old one is started.
 * 
 * @author Luigi Tropiano
 * @author Daniele Parigi
 *
 */
public class RoomList {

    private ServerTable serverTable;
    private Map<Integer, WaitingRoom> rooms;
    private Integer roomKey;
    private String currentMap;
    private WaitingRoom currentRoom;
    private Topic currentTopic;
    private int maxPlayers;
    private int delaySeconds;
    private Timer timer;

    public RoomList(ServerTable serverTable) {
        roomKey = 0;
        this.maxPlayers = 8;
        this.delaySeconds = 15;
        this.rooms = new ConcurrentHashMap<Integer, WaitingRoom>();
        this.currentMap = "Galilei";
        this.serverTable = serverTable;
        this.currentTopic = new Topic();
        newRoom(currentTopic);
    }

    public Integer getRoomKey() {
        return roomKey;
    }

    public Map<Integer, WaitingRoom> getRooms() {
        return rooms;
    }

    /**
     * Add the client to the first available room. When a room gets at least two
     * players it starts the timer. If a room gets full it stops the timer, and
     * call the startGame() method.
     * 
     * @param player
     *            the UUID of the client
     */
    public synchronized void joinRoom(UUID player) {
        currentRoom.getPlayers().add(player);
        serverTable.getClientPlace().put(player, currentRoom);
        currentTopic.publish("A new player joined the room");
        if (!currentRoom.isAvailable()) {
            System.out.println("Resetting timer");
            timer.cancel();
            startGame();
        } else if (currentRoom.hasTwoPlayers())
            startTimer(delaySeconds);
    }

    /**
     * It starts the countdown before executing the task.
     * 
     * @param delaySeconds
     *            the seconds to wait before executing the task
     */
    public void startTimer(int delaySeconds) {
        timer = new Timer();
        timer.schedule(new Task(), delaySeconds * 1000);
    }

    /**
     * Contains the task to be executed from the Timer.
     * 
     * @author Luigi Tropiano
     * @author Daniele Parigi
     *
     */
    class Task extends TimerTask {
        @Override
        public void run() {
            startGame();
        }
    }

    /**
     * It starts the current game calling the startGame() method of the
     * WaitingRoom. Then it create a new Topic and a new Room for the next
     * connections-in
     * 
     */
    public void startGame() {
        currentRoom.startGame();
        // creates the Topic for the next connections
        currentTopic = new Topic();
        newRoom(currentTopic);
    }

    /**
     * Creates a new Room and increments the room counter.
     * 
     * @param topic
     *            the topic related to the new Room
     * @return the new Room
     */
    private void newRoom(Topic topic) {
        roomKey++;
        currentRoom = new WaitingRoom(roomKey, currentMap, maxPlayers, topic,
                serverTable);
        // deprecated
        rooms.put(roomKey, currentRoom);
    }

    public WaitingRoom getCurrentRoom() {
        return currentRoom;
    }

    public Topic getCurrentTopic() {
        return currentTopic;
    }

    @Override
    public String toString() {
        String string = "";
        for (WaitingRoom value : rooms.values()) {
            string = string + value.toString() + "\n";
        }
        return string;
    }

}
