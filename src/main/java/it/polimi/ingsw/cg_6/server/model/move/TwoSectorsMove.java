package it.polimi.ingsw.cg_6.server.model.move;

import it.polimi.ingsw.cg_6.server.model.character.Alien;
import it.polimi.ingsw.cg_6.server.model.character.Player;
import it.polimi.ingsw.cg_6.server.model.map.EscapeHatchSector;
import it.polimi.ingsw.cg_6.server.model.map.Sector;

public class TwoSectorsMove implements Move {

    @Override
    public boolean isValid(Player player, Sector arrival) {
        Sector start = player.getPosition();
        if ((player instanceof Alien) && (arrival instanceof EscapeHatchSector))
            return false;
        if ((arrival instanceof EscapeHatchSector) && !((EscapeHatchSector) arrival).isAvailable())
            return false;
        if (start == arrival)
            return false;
        if (start.getNearSectors().contains(arrival))
            return true;
        for (Sector sector : start.getNearSectors())
            if (sector.getNearSectors().contains(arrival))
                return true;
        return false;
    }

}
