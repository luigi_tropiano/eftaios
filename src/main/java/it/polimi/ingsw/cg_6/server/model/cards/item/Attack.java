package it.polimi.ingsw.cg_6.server.model.cards.item;

import it.polimi.ingsw.cg_6.server.model.GameState;

/**
 * the card Attack used to make the correct ItemDeck
 * 
 * @author Daniele Parigi
 * @author Luigi Tropiano
 *
 */
public class Attack extends ItemCard {

    @Override
    public String toString() {
        return "attack";
    }

    // never called method (use Controller.attack to activate this card)
    @Override
    public String activate(GameState gameState) {
        return "something really strange happens";
    }
}
