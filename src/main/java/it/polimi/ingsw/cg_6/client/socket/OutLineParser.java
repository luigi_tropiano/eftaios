package it.polimi.ingsw.cg_6.client.socket;

import it.polimi.ingsw.cg_6.client.InputConverter;

/**
 * pars the socket client input before sending it to server
 * 
 * @author Daniele Parigi
 * @author Luigi Tropiano
 */
public class OutLineParser {

    private String inLine;

    public OutLineParser(String inLine) {
        this.inLine = inLine;
    }

    /**
     * pars the string with the available command and call method to check the
     * format of the in line
     * 
     * @return the String to send to the Server
     */
    public String parser() {
        String[] split;
        split = inLine.split(" ");
        switch (split[1]) {

        case "chat":
            return chat(inLine);

        case "move":
            return move(split);

        case "draw":
            return draw();

        case "attack":
            return attack();

        case "noise":
            return noise(split);

        case "use":
            return use(split);

        case "end":
            return endTurn();
        case "discard":
            return discard(split);
        default:
            System.out.println("command refused");
            return null;
        }
    }

    /**
     * check the format of the string in case of discard call
     * 
     * @param split
     *            the String[] to check
     * @return the in line if it's ready for server, null if not
     */
    private String discard(String[] split) {
        if (split.length != 3) {
            System.out.println("usage: discard <item>");
            return null;
        }
        return inLine;
    }

    /**
     * check the format of the string in case of use call
     * 
     * @param split
     *            the String[] to check
     * @return the in line if it's ready for server, null if not
     */
    private String use(String[] split) {
        if (split.length < 3) {
            System.out.println("usage: use <item>");
            return null;
        }
        if (split.length > 2 && "spotlight".equalsIgnoreCase(split[2])) {
            return catcher(split, 5, "use spotlight");
        }
        return inLine;
    }

    private String endTurn() {
        return inLine;
    }

    /**
     * check the format of the string in case of noise call
     * 
     * @param split
     *            the String[] to check
     * @return the in line if it's ready for server, null if not
     */
    private String noise(String[] split) {
        return catcher(split, 4, "noise");
    }

    private String attack() {
        return inLine;
    }

    private String draw() {
        return inLine;
    }

    /**
     * check the format of the string in case of move call
     * 
     * @param split
     *            the String[] to check
     * @return the in line if it's ready for server, null if not
     */
    private String move(String[] split) {
        return catcher(split, 4, "move");
    }

    private String chat(String inLine) {
        return inLine;
    }

    /**
     * check the String for other methods
     * 
     * @param split
     *            the String[] to check
     * @param size
     *            the max size of the right string[] that can be send to server
     * @param name
     *            the method that call it for correct return propose
     * @return the in line if it's ready for server, null if not
     */
    private String catcher(String[] split, int size, String name) {
        if (split.length != size || !split[size - 2].matches("\\p{Alpha}")
                || !split[size - 1].matches("\\d(\\d)?")) {
            System.out.println("usage: " + name + " <letter> <number>");
            return null;
        }
        if ("move".equals(name) || "noise".equals(name)) {
            split[2] = Integer.toString(InputConverter.charToInt(split[2]
                    .charAt(0)));
            inLine = split[0] + " " + split[1] + " " + split[2] + " "
                    + split[3];
        }
        return inLine;
    }
}
