package it.polimi.ingsw.cg_6.client.GUI;

import it.polimi.ingsw.cg_6.client.InputConverter;
import it.polimi.ingsw.cg_6.client.socket.SocketCommunicator;
import it.polimi.ingsw.cg_6.server.model.cards.sector.NoiseEverywhere;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.net.Socket;
import java.util.UUID;
import java.util.logging.Level;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.text.DefaultCaret;

/**********************************
 * This is the main class of a Java program to play a game based on hexagonal
 * tiles. The mechanism of handling hexes is in the file Drawer.java.
 * 
 * Adpted from a class written by: M.H. Date: December 2012
 * 
 * @author Luigi Tropiano
 * @author Daniele Parigi
 ***********************************/

public class SocketGUI extends GUI implements Runnable {

    private String ip;
    private int port = 29999;
    private UUID id = UUID.randomUUID();
    private SocketCommunicator communicator;
    private JTextArea chatBox;
    private DrawingPanel panel;
    private int sectortype;
    private int x;
    private int y;

    public SocketGUI(String IP) {
        this.ip = IP;
    }

    @Override
    public void run() {
        initGame();
        createAndShowGUI();
    }

    /**
     * Creates the GUI to play the game. The GUI has a clickable game map, a
     * chat panel for chatting and receiving server messages and a panel with 9
     * input bottons.
     * 
     */
    private void createAndShowGUI() {

        // creating the skeleton panels
        panel = new DrawingPanel();
        JPanel rightPanel = new JPanel();
        JPanel buttons = new JPanel();
        JPanel Upbutton = new JPanel();
        JPanel downbutton = new JPanel();

        // creating the main frame
        JFrame frame = new JFrame("EFTAIOS");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new BorderLayout());

        // creating buttons' panels
        buttons.setLayout(new BorderLayout());
        buttons.add(Upbutton, BorderLayout.NORTH);
        buttons.add(downbutton, BorderLayout.SOUTH);

        // creating item buttons
        Upbutton.setLayout(new FlowLayout());
        Upbutton.add(new AlphaContainer(itemButtonSetup("adrenaline")));
        Upbutton.add(new AlphaContainer(itemButtonSetup("sedatives")));
        Upbutton.add(new AlphaContainer(itemButtonSetup("teleport")));
        Upbutton.add(new AlphaContainer(itemButtonSetup("attack")));
        JButton spotlight = buttonSetup("spotlight");
        Upbutton.add(new AlphaContainer(spotlight));
        spotlight.addActionListener(new SpotlightButtonListener());

        // creating attack, draw, endturn and discard buttons
        JButton attack, draw, endturn, discard;
        downbutton.setLayout(new FlowLayout());
        discard = buttonSetup("discard");
        downbutton.add(new AlphaContainer(discard));
        discard.addActionListener(new DiscardButtonListener());
        attack = buttonSetup("attack1");
        downbutton.add(new AlphaContainer(attack));
        attack.addActionListener(new AttackButtonListener());
        draw = buttonSetup("draw");
        downbutton.add(new AlphaContainer(draw));
        draw.addActionListener(new DrawButtonListener());
        endturn = buttonSetup("endturn");
        downbutton.add(new AlphaContainer(endturn));
        endturn.addActionListener(new EndturnButtonListener());

        // right panel settings
        rightPanel.setLayout(new BorderLayout());
        rightPanel.add(createChatPanel(), BorderLayout.NORTH);
        rightPanel.add(buttons, BorderLayout.CENTER);

        panel.setBackground(new Color(0, 0, 0, 0));

        // adding panel to frame
        frame.add(new AlphaContainer(panel), BorderLayout.CENTER);
        frame.add(rightPanel, BorderLayout.EAST);

        // finalizing frame
        frame.setSize(SCRWIDTH, SCRHEIGHT);
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        panel.repaint();
    }

    /**
     * setup a item button whit icon and action listener
     * 
     * @param line
     *            the name of the button icon
     * @return the JButton created
     */
    public JButton itemButtonSetup(String line) {
        Border emptyBorder = BorderFactory.createEmptyBorder();
        Icon icon = new ImageIcon("res" + File.separatorChar + line + ".png");
        JButton button = new JButton(icon);
        button.setBorder(emptyBorder);
        button.setBackground(COLOURBACK);
        button.setFocusPainted(false);
        button.addActionListener(new ActionItemButtonListener(line));
        return button;
    }

    private JButton sendMessage;

    /**
     * creates a simple chat panel. at the end it starts the pub-sub connection.
     * 
     * @return the chat panel just created
     */
    public JPanel createChatPanel() {
        JPanel backPanel = new JPanel();
        JPanel northPanel = new JPanel();
        JPanel southPanel = new JPanel();
        chatBox = new JTextArea(32, 20);
        JScrollPane scrollChat = new JScrollPane(new AlphaContainer(chatBox));
        messageBox = new JTextField();
        sendMessage = new JButton("Send");
        sendMessage.addActionListener(new sendMessageButtonListener());

        // jtextArea setting
        chatBox.setEditable(false);
        chatBox.setFont(new Font("Serif", Font.PLAIN, 15));
        chatBox.setLineWrap(true);
        chatBox.setBackground(new Color(0, 0, 0, 30));
        chatBox.setForeground(Color.BLACK);
        chatBox.setWrapStyleWord(true);
        DefaultCaret caret = (DefaultCaret) chatBox.getCaret();
        caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
        // putting chatBox in northPanel
        northPanel.setLayout(new BorderLayout());
        northPanel.setOpaque(false);
        // jtextField setting
        messageBox.requestFocusInWindow();
        messageBox.setBorder(BorderFactory.createLineBorder(Color.WHITE, 1));
        messageBox.setOpaque(false);
        messageBox.setForeground(Color.BLACK);
        // southPanel setting
        southPanel.setLayout(new GridBagLayout());
        southPanel.setBackground(new Color(0, 0, 0, 70));
        southPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        GridBagConstraints left = new GridBagConstraints();
        left.anchor = GridBagConstraints.LINE_START;
        left.fill = GridBagConstraints.HORIZONTAL;
        left.weightx = 512.0D;
        left.weighty = 1.0D;

        GridBagConstraints right = new GridBagConstraints();
        right.insets = new Insets(0, 10, 0, 0);
        right.anchor = GridBagConstraints.LINE_END;
        right.fill = GridBagConstraints.NONE;
        right.weightx = 1.0D;
        right.weighty = 1.0D;

        northPanel.add(scrollChat, BorderLayout.CENTER);

        southPanel.add(messageBox, left);
        southPanel.add(sendMessage, right);

        scrollChat.setOpaque(false);
        scrollChat.getViewport().setOpaque(false);
        scrollChat.setBorder(null);

        // making the textbox capable of receiving server messages
        backPanel.setBackground(Color.WHITE);
        backPanel.setLayout(new BorderLayout());
        backPanel.add(northPanel, BorderLayout.CENTER);
        backPanel.add(new AlphaContainer(southPanel), BorderLayout.SOUTH);

        // starting pub-sub connection
        send("join");
        SubscriberThreadGUI subscriber = new SubscriberThreadGUI(ip, id,
                chatBox);
        subscriber.start();
        return backPanel;
    }

    private boolean notTheFirstTime;

    /**
     * creates a channel to communicate with the server through sockets. Then
     * sends the massage to the server and wait for his response.
     * 
     * @param line
     *            the message to be sent
     */
    public void send(String line) {
        try {
            Socket socket = new Socket(ip, port);
            communicator = new SocketCommunicator(socket);
            communicator.send(id + " " + line);
            String inLine = communicator.receive();
            chatBox.append(inLine + "\n");
            if (inLine.contains(GUI.YOUMOVEDINTO)) {
                if (notTheFirstTime)
                    board[x][y] = sectortype;
                String[] split = inLine.split(" ");
                sectortype = board[InputConverter
                        .charToInt((split[split.length - 2]).charAt(0)) - 1][Integer
                        .parseInt(split[split.length - 1]) - 1];
                x = InputConverter.charToInt((split[split.length - 2])
                        .charAt(0)) - 1;
                y = Integer.parseInt(split[split.length - 1]) - 1;
                board[InputConverter.charToInt((split[split.length - 2])
                        .charAt(0)) - 1][Integer
                        .parseInt(split[split.length - 1]) - 1] = sectortype + 1;
                panel.repaint();
                notTheFirstTime = true;

            } else if (inLine.contains(NoiseEverywhere.CHOOSENOISE))
                setNoise(true);
            communicator.close();
        } catch (IOException e) {
            logger.log(Level.SEVERE, "send broken", e);
        }
    }

    /**
     * Button listener to send messages to chat
     */
    class sendMessageButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (messageBox.getText().length() < 1) {
                // do nothing
            } else if (".clear".equals(messageBox.getText())) {
                chatBox.setText("Cleared all messages\n");
                messageBox.setText("");
            } else {
                send("chat " + messageBox.getText() + "\n");
            }
            messageBox.setText("");
            messageBox.requestFocusInWindow();
        }
    }

    /**
     * This is the button listener of all the item buttons
     */
    class ActionItemButtonListener implements ActionListener {

        String line;

        public ActionItemButtonListener(String line) {
            this.line = "use " + line;
        }

        @Override
        public void actionPerformed(ActionEvent event) {
            send(line);
        }
    }

    /**
     * button listener for spotlight: it simply sets a boolean to advise the
     * mouse listener of the map to send a 'use spotlight'
     */
    class SpotlightButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent event) {
            setSpotlight(true);
        }
    }

    /**
     * it creates a frame to choose which item to discard
     */
    class DiscardButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent event) {
            if (!isBottunDisabler()) {
                setBottunDisabler(true);
                JFrame popup = new JFrame("Discard");
                popup.setSize(200, 110);
                popup.setUndecorated(true);
                popup.setResizable(false);
                popup.setLocationRelativeTo(null);
                popup.setVisible(true);
                popup.setLayout(new FlowLayout());
                popup.add(new AlphaContainer(discardButtonSetup("adrenaline",
                        popup)));
                popup.add(new AlphaContainer(discardButtonSetup("sedatives",
                        popup)));
                popup.add(new AlphaContainer(discardButtonSetup("teleport",
                        popup)));
                popup.add(new AlphaContainer(
                        discardButtonSetup("attack", popup)));
                popup.add(new AlphaContainer(discardButtonSetup("spotlight",
                        popup)));
                popup.add(new AlphaContainer(discardButtonSetup("defense",
                        popup)));
            }
        }
    }

    /**
     * it creates one button for a popup frame
     * 
     * @param line
     *            the name of the icon to pick
     * @param popup
     *            the frame
     * @return the button just created
     */
    public JButton discardButtonSetup(String line, JFrame popup) {
        Border emptyBorder = BorderFactory.createEmptyBorder();
        Icon icon = new ImageIcon("res" + File.separatorChar + line + ".png");
        JButton button = new JButton(icon);
        button.setBorder(emptyBorder);
        button.setBackground(COLOURBACK);
        button.setFocusPainted(false);
        button.addActionListener(new DiscardItemButtonListener(line, popup));
        return button;
    }

    /**
     * The listener of discard-item button
     */
    class DiscardItemButtonListener implements ActionListener {
        private String line;
        private JFrame popup;

        public DiscardItemButtonListener(String line, JFrame popup) {
            this.line = "discard " + line;
            this.popup = popup;
        }

        @Override
        public void actionPerformed(ActionEvent event) {
            send(line);
            popup.dispose();
            setBottunDisabler(false);
        }
    }

    /**
     * The listener of attack button
     */
    class AttackButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent event) {
            send("attack");
        }
    }

    /**
     * The listener of draw button
     */
    class DrawButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent event) {
            send("draw");
        }
    }

    /**
     * The listener of endturn button
     */
    class EndturnButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent event) {
            send("end");
        }
    }

    /**
     * Draw the hexagonal grid and fill the hexagon related to the map to build.
     */
    class DrawingPanel extends JPanel {

        private static final long serialVersionUID = 1L;

        public DrawingPanel() {

            MyMouseListener ml = new MyMouseListener();
            addMouseListener(ml);
            repaint();
        }

        @Override
        public void paintComponent(Graphics g) {
            Graphics2D g2 = (Graphics2D) g;
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                    RenderingHints.VALUE_ANTIALIAS_ON);
            g.setFont(new Font("TimesRoman", Font.PLAIN, 11));
            super.paintComponent(g2);
            // draw grid
            for (int i = 0; i < BSIZEX; i++) {
                for (int j = 0; j < BSIZEY; j++) {
                    Drawer.drawHex(i, j, g2);
                }
            }
            // fill in hexes
            for (int i = 0; i < BSIZEX; i++) {
                for (int j = 0; j < BSIZEY; j++) {
                    Drawer.fillHex(i, j, board[i][j], g2);
                }
            }

            // g.setColor(Color.RED);
            // g.drawLine(mPt.x,mPt.y, mPt.x,mPt.y);
        }

        class MyMouseListener extends MouseAdapter { // inner class inside

            public MyMouseListener() {
            }

            // DrawingPanel
            @Override
            public void mouseClicked(MouseEvent e) {

                Point p = new Point(Drawer.pxtoHex(e.getX(), e.getY()));
                if (p.x < 0 || p.y < 0 || p.x >= BSIZEX || p.y >= BSIZEY)
                    return;

                // Setting what to do when a hexagon is clicked
                if (board[p.x][p.y] != EMPTY) {
                    repaint();
                    int a = p.x + 1;
                    int b = p.y + 1;
                    if (isNoise()) {
                        send("noise " + a + " " + b);
                        setNoise(false);
                    } else if (isSpotlight()) {
                        send("use spotlight " + a + " " + b);
                        setSpotlight(false);
                    } else {
                        send("move " + a + " " + b);
                    }
                }
            }
        } // end of MyMouseListener class
    } // end of DrawingPanel class
}