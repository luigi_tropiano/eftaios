package it.polimi.ingsw.cg_6.server.controller;

import it.polimi.ingsw.cg_6.server.model.GameState;
import it.polimi.ingsw.cg_6.server.model.character.Player;
import it.polimi.ingsw.cg_6.server.rmi.BrokerInterface;

import java.rmi.RemoteException;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * organizes the client's positions in the server (client in Waiting Room or
 * GameState) and associates the client UUID with the player
 * 
 * @author Daniele Parigi
 * @author Luigi Tropiano
 *
 */
public class ServerTable implements BrokerInterface {

    private GameList gameList;
    private RoomList roomList;
    private Map<UUID, Object> clientPlace;
    private Map<UUID, Player> clientPlayerTable;

    public ServerTable() {
        this.gameList = new GameList();
        this.roomList = new RoomList(this);
        // associates Client UUID with the GameState or the WaitingRoom
        this.clientPlace = new ConcurrentHashMap<UUID, Object>();
        // associated Client UUID with the Player
        this.clientPlayerTable = new ConcurrentHashMap<UUID, Player>();
    }

    public Map<UUID, Player> getClientPlayerTable() {
        return clientPlayerTable;
    }

    public GameList getGameList() {
        return gameList;
    }

    public RoomList getRoomList() {
        return roomList;
    }

    public Map<UUID, Object> getClientPlace() {
        return clientPlace;
    }

    /**
     * 
     * @param id
     *            Client ID
     * @return the GameState or the Waiting Room in which the client is
     */
    public Object locate(UUID id) {
        switch (this.playerStatus(id)) {
        case INROOM:
            return ((WaitingRoom) clientPlace.get(id));
        case INGAME:
            return ((GameState) clientPlace.get(id));
        default:
            return null;
        }
    }

    /**
     * stabilizes the state of the client
     * 
     * @param id
     *            the client ID
     * @return OUTROOM, INGAME, INROOM (where the client is)
     */
    private PlayerStatus playerStatus(UUID id) {
        if (clientPlace.get(id) == null)
            return PlayerStatus.OUTROOM;
        else if (clientPlace.get(id) instanceof GameState)
            return PlayerStatus.INGAME;
        else
            return PlayerStatus.INROOM;
    }

    /**
     * call the method subscribe() of the topic to subscribe the client to the
     * right topic for the Pub-Sub
     * 
     * @param id
     *            the client id
     * @param subscriber
     *            if RMI connection it's a SubscriberInterface, if Socket it's a
     *            BrokerTread
     * @throws RemoteException
     */
    @Override
    public void subscribe(UUID id, Object subscriber) throws RemoteException {
        if (locate(id) instanceof WaitingRoom)
            ((WaitingRoom) locate(id)).getTopic().subscribe(id, subscriber);
        if (locate(id) instanceof GameState)
            ((GameState) locate(id)).getPlayerQueue().getTopic()
                    .subscribe(id, subscriber);
    }
}