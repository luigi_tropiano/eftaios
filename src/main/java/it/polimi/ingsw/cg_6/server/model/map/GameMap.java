package it.polimi.ingsw.cg_6.server.model.map;

import java.awt.Point;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GameMap {
    private Sector alienSector;
    private Sector humanSector;
    private Logger logger = Logger.getLogger("escape");

    private Map<Point, Sector> sectors;

    public GameMap() {
        sectors = new ConcurrentHashMap<Point, Sector>();

    }

    public void setAlienSector(Sector alienSector) {
        this.alienSector = alienSector;
    }

    public Sector getAlienSector() {
        return alienSector;
    }

    public void setHumanSector(Sector humanSector) {
        this.humanSector = humanSector;
    }

    public Sector getHumanSector() {
        return humanSector;
    }

    public void addSector(Point point, Sector sector) {
        this.sectors.put(point, sector);
    }

    public Sector getSector(Point point) {
        return sectors.get(point);
    }

    /**
     * Costruisce l'insieme di settori vicini al settore passato come parametro
     * e glielo passa alla setNearSectors di quest'ultimo.
     * 
     * @param sector
     */

    public void buildNearSectors(Sector sector) {
        int x = sector.getPoint().x;
        int y = sector.getPoint().y;

        try {
            sector.addNearSector(sectors.get(new Point(x, y + 1)));
            sector.addNearSector(sectors.get(new Point(x, y - 1)));
            sector.addNearSector(sectors.get(new Point(x - 1, y - (x % 2))));
            sector.addNearSector(sectors.get(new Point(x + 1, y - (x % 2))));
            sector.addNearSector(sectors
                    .get(new Point(x - 1, y + ((x + 1) % 2))));
            sector.addNearSector(sectors
                    .get(new Point(x + 1, y + ((x + 1) % 2))));
        } catch (NullPointerException nullPointer) {
            logger.log(Level.SEVERE, "Error in sector " + x + " " + y,
                    nullPointer);
        }
    }

    public boolean isThereAnyAvailableHatch() {
        for (Sector sector : sectors.values())
            if ((sector instanceof EscapeHatchSector)
                    && ((EscapeHatchSector) sector).isAvailable())
                return true;
        return false;
    }

    public Map<Point, Sector> getSectors() {
        return sectors;
    }
}
