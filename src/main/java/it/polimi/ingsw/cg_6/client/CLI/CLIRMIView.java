package it.polimi.ingsw.cg_6.client.CLI;

import it.polimi.ingsw.cg_6.client.rmi.RMIClient;

public class CLIRMIView extends CLI{
    
    private CLIRMIView() {
    }

    public static void start() {

        String HOST = waitForIP();
        int PORT = 1099;
        RMIClient client = new RMIClient(HOST, PORT);
        new Thread(client).start();
    }
}
