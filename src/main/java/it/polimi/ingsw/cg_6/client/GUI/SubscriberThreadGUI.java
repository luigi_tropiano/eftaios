package it.polimi.ingsw.cg_6.client.GUI;

import it.polimi.ingsw.cg_6.client.socket.SubscriberThread;

import java.io.IOException;
import java.util.UUID;
import java.util.logging.Level;

import javax.swing.JTextArea;

public class SubscriberThreadGUI extends SubscriberThread {

    JTextArea chatbox;

    public SubscriberThreadGUI(String IP, UUID id, JTextArea chatBox) {
        super(IP, id);
        this.chatbox = chatBox;
    }

    /**
     * Metodo che riceve eventuali messaggi di testo dal publisher
     * 
     * @return the String received
     */
    @Override
    protected void receive() {
        String msg = null;
        try {
            msg = in.readLine();
            if (msg != null) {
                chatbox.append(msg + "\n");
            }
        } catch (IOException e) {
            logger.log(Level.SEVERE, "receive", e);
        }
    }
}
