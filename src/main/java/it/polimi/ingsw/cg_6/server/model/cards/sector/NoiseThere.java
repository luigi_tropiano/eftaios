package it.polimi.ingsw.cg_6.server.model.cards.sector;

import it.polimi.ingsw.cg_6.client.InputConverter;
import it.polimi.ingsw.cg_6.server.model.GameState;
import it.polimi.ingsw.cg_6.server.model.character.Player;

/**
 * The noise there card
 * 
 * @author Daniele Parigi
 * @author Luigi Tropiano
 */
public class NoiseThere extends SectorCard {
    /**
     * activate of the Noise There card and log the result
     * 
     * @param player
     * @param gameState
     * @return where the player make noise
     */
    @Override
    public String activate(Player player, GameState gameState) {
        gameState.addLog(player.toString() + " draws a sector card");
        gameState.addLog(player.toString() + " makes noise in sector "
                + InputConverter.intToChar(player.getPosition().getPoint().x)
                + " " + player.getPosition().getPoint().y);
        return "you made noise in sector "
                + InputConverter.intToChar(player.getPosition().getPoint().x)
                + " " + player.getPosition().getPoint().y;
    }
}
