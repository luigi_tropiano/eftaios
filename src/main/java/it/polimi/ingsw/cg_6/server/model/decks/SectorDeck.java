package it.polimi.ingsw.cg_6.server.model.decks;

import it.polimi.ingsw.cg_6.server.model.cards.sector.NoiseEverywhere;
import it.polimi.ingsw.cg_6.server.model.cards.sector.NoiseEverywhereObj;
import it.polimi.ingsw.cg_6.server.model.cards.sector.NoiseThere;
import it.polimi.ingsw.cg_6.server.model.cards.sector.NoiseThereObj;
import it.polimi.ingsw.cg_6.server.model.cards.sector.Silence;

public class SectorDeck extends Deck {

    public SectorDeck() {

        for (int i = 6; i > 0; i--) {
            addCard(new NoiseThere());
            addCard(new NoiseEverywhere());
        }
        for (int i = 4; i > 0; i--) {
            addCard(new NoiseThereObj());
            addCard(new NoiseEverywhereObj());
        }
        for (int i = 5; i > 0; i--)
            addCard(new Silence());

        shuffleDeck();
    }
}
