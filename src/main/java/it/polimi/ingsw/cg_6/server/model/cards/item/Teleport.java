package it.polimi.ingsw.cg_6.server.model.cards.item;

import it.polimi.ingsw.cg_6.server.model.GameState;

/**
 * the card teleport
 * 
 * @author Daniele Parigi
 * @author Luigi Tropiano
 *
 */
public class Teleport extends ItemCard {
    @Override
    public String toString() {
        return "teleport";
    }

    /**
     * teleport the player to the human sector, updates the log, discards the
     * card and removes it from the player hand
     * 
     * @param gameState
     * @return The String to describe action performed
     */
    @Override
    public String activate(GameState gameState) {
        gameState.getCurrentPlayer().setInitialPosition(gameState.getGameMap());
        gameState.addLog(gameState.getCurrentPlayer().toString()
                + " uses item [" + this.toString() + "]");
        gameState.getItemDeck().discardCard(this);
        gameState.getCurrentPlayer().getItems().remove(this);
        return "you teleported to the human sector";
    }
}
