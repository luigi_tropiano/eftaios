package it.polimi.ingsw.cg_6.client.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface SubscriberInterface extends Remote {

    public void dispatchMessage(String msg) throws RemoteException;

}