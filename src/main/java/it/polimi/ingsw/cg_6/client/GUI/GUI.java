package it.polimi.ingsw.cg_6.client.GUI;

import it.polimi.ingsw.cg_6.client.rmi.RMIClient;
import it.polimi.ingsw.cg_6.server.model.map.AlienSector;
import it.polimi.ingsw.cg_6.server.model.map.DangerousSector;
import it.polimi.ingsw.cg_6.server.model.map.EscapeHatchSector;
import it.polimi.ingsw.cg_6.server.model.map.GameMap;
import it.polimi.ingsw.cg_6.server.model.map.HumanSector;
import it.polimi.ingsw.cg_6.server.model.map.MapParser;
import it.polimi.ingsw.cg_6.server.model.map.Sector;
import it.polimi.ingsw.cg_6.server.model.map.SecureSector;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

/**
 * The class that launch the RMI and Socket GUI
 * 
 * @author Daniele Parigi
 * @author Luigi Tropiano
 */
public class GUI {

    static final Color COLOURBACK = new Color(255, 255, 255, 0);
    protected static final Color COLOURCELL = new Color(255, 255, 255, 0);
    protected static final Color COLOURGRID = Color.BLACK;
    protected static final Color COLORDANGEROUS = new Color(211, 211, 211, 255);
    protected static final Color COLORDANGEROUSTXT = new Color(0, 0, 0, 230);
    protected static final Color COLORSECURE = new Color(255, 255, 255, 200);
    protected static final Color COLORSECURETXT = new Color(0, 0, 0, 230);
    protected static final Color COLORESCAPE = new Color(255, 255, 255, 255);
    protected static final Color COLORESCAPETXT = Color.WHITE;
    protected static final Color COLORALIEN = new Color(255, 0, 70, 255);
    protected static final Color COLORHUMAN = new Color(50, 100, 255, 255);
    protected static final String YOUMOVEDINTO = "you moved into sector";
    protected static final int EMPTY = 0;
    protected static final int BSIZEX = 23;
    protected static final int BSIZEY = 14;
    protected static final int HEXSIZE = 50;
    protected static final int BORDERS = 15;
    protected static final int SCRWIDTH = HEXSIZE * (BSIZEX + 3) + BORDERS;
    protected static final int SCRHEIGHT = HEXSIZE * (BSIZEY + 1) + BORDERS * 2;
    public static final Logger logger = Logger.getLogger("escape");
    protected JTextField messageBox;
    protected JTextArea chatBox;
    protected int[][] board = new int[BSIZEX][BSIZEY];
    JTextArea textArea = new JTextArea(5, 10);

    private boolean noise;
    private boolean spotlight;
    private boolean bottunDisabler = false;

    private static JFrame launcher = new JFrame("Insert IP");
    private static JTextField ipPanel = new JTextField();

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                GUI.createAndShowLauncher();
            }
        });
    }

    /**
     * create and show a launcher to choose type of connection(RMI and GUI) and
     * the IP of the server after pressing the bottom the real game GUI start
     */
    public static void createAndShowLauncher() {
        JPanel abovePanel = new JPanel();
        JPanel choose = new JPanel();
        ipPanel.setBorder(BorderFactory.createLineBorder(Color.WHITE, 2));
        launcher.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        launcher.setLayout(new BorderLayout());
        launcher.add(choose, BorderLayout.CENTER);
        launcher.add(abovePanel, BorderLayout.NORTH);
        abovePanel.setLayout(new BorderLayout());
        abovePanel.add(ipPanel, BorderLayout.NORTH);
        abovePanel.setBorder(BorderFactory.createEmptyBorder(20, 20, 10, 20));
        choose.setLayout(new FlowLayout());
        JButton socket = buttonSetup("socket");
        JButton rMI = buttonSetup("rmi");
        choose.add(socket, BorderLayout.EAST);
        choose.add(rMI, BorderLayout.WEST);
        socket.addActionListener(new SocketButtonListener(launcher, ipPanel));
        rMI.addActionListener(new RMIButtonListener(launcher, ipPanel));
        launcher.setSize(400, 220);
        launcher.setResizable(false);
        launcher.setLocationRelativeTo(null);
        launcher.setVisible(true);
    }

    /**
     * Put in the hexagon grid a int that represent the type of sector in the
     * map (parsed in this sector)
     */
    public void initGame() {

        Drawer.setXYasVertex(false); // RECOMMENDED: leave this as FALSE.

        Drawer.setHeight(HEXSIZE); // Either setHeight or setSize must be run
                                   // to initialize the hex
        Drawer.setBorders(BORDERS);

        for (int i = 0; i < BSIZEX; i++) {
            for (int j = 0; j < BSIZEY; j++) {
                board[i][j] = EMPTY;
            }
        }

        // build the map
        MapParser mp = new MapParser();
        try {
            mp.createMap("Galilei");
        } catch (IOException | SAXException | ParserConfigurationException e) {
            logger.log(Level.SEVERE, "map problem", e);
        }
        // set some int in the board defining the type of sector
        GameMap map = mp.getMap();
        for (Sector sector : map.getSectors().values()) {
            if (sector instanceof DangerousSector)
                board[sector.getPoint().x - 1][sector.getPoint().y - 1] = (int) 'D';
            if (sector instanceof SecureSector)
                board[sector.getPoint().x - 1][sector.getPoint().y - 1] = (int) 'S';
            if (sector instanceof EscapeHatchSector)
                board[sector.getPoint().x - 1][sector.getPoint().y - 1] = (int) 'W';
            if (sector instanceof AlienSector)
                board[sector.getPoint().x - 1][sector.getPoint().y - 1] = (int) 'A';
            if (sector instanceof HumanSector)
                board[sector.getPoint().x - 1][sector.getPoint().y - 1] = (int) 'H';
        }
    }

    /**
     * setup a basic button whit icon
     * 
     * @param line
     *            the name of the button icon
     * @return the JButton created
     */
    public static JButton buttonSetup(String line) {
        Border emptyBorder = BorderFactory.createEmptyBorder();
        Icon icon = new ImageIcon("res" + File.separatorChar + line + ".png");
        JButton button = new JButton(icon);
        button.setBorder(emptyBorder);
        button.setBackground(COLOURBACK);
        button.setFocusPainted(false);
        return button;
    }

    public boolean isNoise() {
        return noise;
    }

    public void setNoise(boolean noise) {
        this.noise = noise;
    }

    public boolean isSpotlight() {
        return spotlight;
    }

    public void setSpotlight(boolean spotlight) {
        this.spotlight = spotlight;
    }

    public GUI() {
        super();
    }

    public boolean isBottunDisabler() {
        return bottunDisabler;
    }

    public void setBottunDisabler(boolean bottunDisabler) {
        this.bottunDisabler = bottunDisabler;
    }

}

/**
 * button listener for the socket button
 * 
 * @author Daniele Parigi
 * @author Luigi Tropiano
 */
class SocketButtonListener implements ActionListener {

    private JFrame launcher;
    private JTextField ipPanel;

    public SocketButtonListener(JFrame launcher, JTextField ipPanel) {
        this.launcher = launcher;
        this.ipPanel = ipPanel;
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        SocketGUI gui = new SocketGUI(ipPanel.getText());
        SwingUtilities.invokeLater(gui);
        launcher.dispose();
    }
}
/**
 * button listener for the RMI button
 * 
 * @author Daniele Parigi
 * @author Luigi Tropiano
 */
class RMIButtonListener implements ActionListener {
    private JFrame launcher;
    private JTextField ipPanel;

    public RMIButtonListener(JFrame launcher, JTextField ipPanel) {
        this.launcher = launcher;
        this.ipPanel = ipPanel;
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        RMIClient client = new RMIClient(ipPanel.getText(), 1099);
        new Thread(client).start();
        RMIGUI gui = new RMIGUI(client);
        SwingUtilities.invokeLater(gui);
        launcher.dispose();
    }
}