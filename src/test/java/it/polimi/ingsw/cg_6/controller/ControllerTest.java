package it.polimi.ingsw.cg_6.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import it.polimi.ingsw.cg_6.server.Topic;
import it.polimi.ingsw.cg_6.server.controller.Controller;
import it.polimi.ingsw.cg_6.server.controller.ServerTable;
import it.polimi.ingsw.cg_6.server.model.GameState;
import it.polimi.ingsw.cg_6.server.model.cards.item.Adrenaline;
import it.polimi.ingsw.cg_6.server.model.cards.item.Attack;
import it.polimi.ingsw.cg_6.server.model.cards.item.Defense;
import it.polimi.ingsw.cg_6.server.model.cards.item.ItemCard;
import it.polimi.ingsw.cg_6.server.model.cards.item.Spotlight;
import it.polimi.ingsw.cg_6.server.model.cards.item.Teleport;
import it.polimi.ingsw.cg_6.server.model.character.Alien;
import it.polimi.ingsw.cg_6.server.model.character.Human;
import it.polimi.ingsw.cg_6.server.model.character.Player;
import it.polimi.ingsw.cg_6.server.model.character.PlayersQueue;
import it.polimi.ingsw.cg_6.server.model.map.GameMap;
import it.polimi.ingsw.cg_6.server.model.map.MapParser;
import it.polimi.ingsw.cg_6.server.model.move.TwoSectorsMove;

import java.awt.Point;
import java.io.IOException;
import java.util.UUID;

import javax.xml.parsers.ParserConfigurationException;

import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

public class ControllerTest {

    private Player player1, player2, player3, player4;
    private Topic topic;
    private GameState gameState;
    private MapParser mp;
    private GameMap gameMap;
    private PlayersQueue playersQueue;
    private int turnLength;
    private Controller controller;
    private ServerTable serverTable;

    @Before
    public void init() throws IOException, SAXException,
            ParserConfigurationException {
        turnLength = 180;
        player1 = new Alien(UUID.randomUUID());
        player2 = new Alien(UUID.randomUUID());
        player3 = new Human(UUID.randomUUID());
        player4 = new Human(UUID.randomUUID());
        mp = new MapParser();
        mp.createMap("Galilei");
        gameMap = mp.getMap();
        topic = new Topic();
        playersQueue = new PlayersQueue(topic);

        playersQueue.addPlayer(player1);
        playersQueue.addPlayer(player2);
        playersQueue.addPlayer(player3);
        playersQueue.addPlayer(player4);
        gameState = new GameState(gameMap, playersQueue, turnLength);

        serverTable = new ServerTable();
        serverTable.getClientPlace().put(player1.getClientId(), gameState);
        serverTable.getClientPlace().put(player2.getClientId(), gameState);
        serverTable.getClientPlace().put(player3.getClientId(), gameState);
        serverTable.getClientPlace().put(player4.getClientId(), gameState);
        serverTable.getClientPlayerTable().put(player1.getClientId(), player1);
        serverTable.getClientPlayerTable().put(player2.getClientId(), player2);
        serverTable.getClientPlayerTable().put(player3.getClientId(), player3);
        serverTable.getClientPlayerTable().put(player4.getClientId(), player4);
        controller = new Controller(serverTable);
    }

    @Test
    public void testUse() {
        String result;

        // Game not started yet
        result = controller.use(UUID.randomUUID(), "use attack");
        assertEquals(Controller.gameNotStarted, result);

        // ALIEN
        gameState.setCurrentPlayer(player1);
        player1.setPosition(gameMap.getSector(new Point(5, 9)));
        player1.addItem(new Attack());
        result = controller.use(player1.getClientId(), player1.getClientId()
                + " use attack");
        assertEquals(Controller.noItemsForYou, result);

        // HUMAN
        player3.setPosition(gameMap.getSector(new Point(5, 8)));
        player3.addItem(new Adrenaline());
        player3.addItem(new Attack());
        player3.addItem(new Teleport());
        player3.addItem(new Spotlight());
        // adding player4 to the same position where player3 will attack. he
        // will use 'defense'
        player4.setPosition(gameMap.getSector(new Point(5, 9)));
        player4.addItem(new Defense());

        // when is not your turn
        result = controller.use(player3.getClientId(), " use adrenaline");
        assertEquals(Controller.notYourTurn, result);
        // when is your turn
        gameState.setCurrentPlayer(player3);
        controller.use(player3.getClientId(), player3.getClientId()
                + " use adrenaline");
        assertTrue(player3.getMove() instanceof TwoSectorsMove);
        // you have to move before you can attack
        controller.move(player3.getClientId(), new Point(5, 9));
        controller.use(player3.getClientId(), player3.getClientId()
                + " use attack");
        assertTrue(player3.getDidAttack());
        assertTrue(playersQueue.getRemovedPlayers().contains(player1));
        assertFalse(playersQueue.getRemovedPlayers().contains(player4));
        assertFalse(playersQueue.getPlayers().contains(player1));
        assertTrue(playersQueue.getPlayers().contains(player4));
        controller.use(player3.getClientId(), player3.getClientId()
                + " use teleport");
        assertEquals(gameMap.getHumanSector(), player3.getPosition());
        controller.use(player3.getClientId(), player3.getClientId()
                + " use spotlight 5 9");
        assertEquals(5, gameState.getItemDeck().getDiscardedCards().size());
    }

    @Test
    public void testMove() {
        String result;

        // Game not started yet
        result = controller.move(UUID.randomUUID(), new Point(5, 5));
        assertEquals(Controller.gameNotStarted, result);

        // ALIEN
        gameState.setCurrentPlayer(player1);
        player1.setPosition(gameMap.getSector(new Point(5, 9)));
        controller.move(player1.getClientId(), new Point(5, 11));
        assertEquals(player1.getPosition(), gameMap.getSector(new Point(5, 11)));
        // player1 has already moved, his position shouldn't change
        controller.move(player1.getClientId(), new Point(5, 9));
        assertEquals(player1.getPosition(), gameMap.getSector(new Point(5, 11)));
        player1.resetFlags();
        // trying to move in an unreachable sector, his position shouldn't
        // change
        controller.move(player1.getClientId(), new Point(11, 9));
        assertEquals(player1.getPosition(), gameMap.getSector(new Point(5, 11)));

        // HUMAN
        gameState.setCurrentPlayer(player3);
        player3.setPosition(gameMap.getSector(new Point(5, 9)));
        // trying to move in an unreachable sector, his position shouldn't
        // change
        controller.move(player3.getClientId(), new Point(5, 11));
        assertEquals(player3.getPosition(), gameMap.getSector(new Point(5, 9)));
        // moving in a reachable sector
        controller.move(player3.getClientId(), new Point(5, 10));
        assertEquals(player3.getPosition(), gameMap.getSector(new Point(5, 10)));
        // player3 has already moved, his position shouldn't change
        controller.move(player3.getClientId(), new Point(5, 9));
        assertEquals(player3.getPosition(), gameMap.getSector(new Point(5, 10)));
        // resetting flag to move again
        player3.resetFlags();
        // trying to move in an unreachable sector, his position shouldn't
        // change
        controller.move(player3.getClientId(), new Point(11, 9));
        assertEquals(player3.getPosition(), gameMap.getSector(new Point(5, 10)));

        // trying to move when is not your turn
        result = controller.move(player1.getClientId(), new Point(4, 12));
        assertEquals(Controller.notYourTurn, result);
    }

    @Test
    public void testDraw() {
        String result;

        // Game not started yet
        result = controller.draw(UUID.randomUUID());
        assertEquals(Controller.gameNotStarted, result);

        gameState.setCurrentPlayer(player1);
        player1.setPosition(gameMap.getSector(new Point(5, 9)));
        // trying to draw in a dangerous sector without moving first
        result = controller.draw(player1.getClientId());
        assertEquals(Controller.moveFirst, result);
        assertEquals(0, gameState.getSectorDeck().getDiscardedCards().size());
        // trying to draw in a dangerous sector after moving
        controller.move(player1.getClientId(), new Point(5, 10));
        controller.draw(player1.getClientId());
        assertEquals(1, gameState.getSectorDeck().getDiscardedCards().size());
        // trying to draw a second time
        result = controller.draw(player1.getClientId());
        assertEquals(Controller.alreadyDrawn, result);
        assertEquals(1, gameState.getSectorDeck().getDiscardedCards().size());

        // resetting flags
        player1.resetFlags();

        player1.setPosition(gameMap.getSector(new Point(11, 9)));
        // trying to draw in a secure sector without moving first
        result = controller.draw(player1.getClientId());
        assertEquals(Controller.moveFirst, result);
        assertEquals(1, gameState.getSectorDeck().getDiscardedCards().size());
        // trying to draw in a secure sector after moving
        controller.move(player1.getClientId(), new Point(12, 9));
        result = controller.draw(player1.getClientId());
        assertEquals(Controller.cantDrawHere, result);
        assertEquals(1, gameState.getSectorDeck().getDiscardedCards().size());

        player2.setPosition(gameMap.getSector(new Point(5, 5)));
        // trying to draw when is not your turn
        result = controller.draw(player2.getClientId());
        assertEquals(Controller.notYourTurn, result);
    }

    @Test
    public void testNoise() {
        String result;

        // Game not started yet
        result = controller.noise(UUID.randomUUID(), new Point(11, 11));
        assertEquals(Controller.gameNotStarted, result);

        gameState.setCurrentPlayer(player1);
        player1.setPosition(gameMap.getSector(new Point(5, 9)));
        // Not your turn
        result = controller.noise(player2.getClientId(), new Point(11, 11));
        assertEquals(Controller.notYourTurn, result);
        // check if made noise
        controller.noise(player1.getClientId(), new Point(11, 11));
        assertEquals(false, player1.getHaveToNoise());
    }

    @Test
    public void testDiscardItem() {
        String result;
        UUID id = UUID.randomUUID();

        // Game not started yet
        result = controller.discardItem(id, id + " discard attack");
        assertEquals(Controller.gameNotStarted, result);

        gameState.setCurrentPlayer(player1);
        player1.setPosition(gameMap.getSector(new Point(5, 9)));
        // Not your turn
        result = controller.discardItem(player2.getClientId(),
                player2.getClientId() + " discard attack");
        assertEquals(Controller.notYourTurn, result);
        // check if item discarded
        ItemCard attack = new Attack();
        ItemCard defense = new Defense();
        ItemCard spotlight = new Spotlight();
        player1.addItem(attack);
        player1.addItem(defense);
        player1.addItem(spotlight);
        assertTrue(player1.getItems().contains(attack));
        controller.discardItem(player1.getClientId(), player1.getClientId()
                + " discard attack");
        assertEquals(2, player1.getItems().size());
        assertFalse(player1.getItems().contains(attack));
        assertTrue(player1.getItems().contains(defense));
        // if more than three items no need to discard
        controller.discardItem(player1.getClientId(), player1.getClientId()
                + " discard defense");
        assertEquals(2, player1.getItems().size());

    }

    @Test
    public void testAttack() {
        String result;

        // Game not started yet
        result = controller.attack(UUID.randomUUID());
        assertEquals(Controller.gameNotStarted, result);

        gameState.setCurrentPlayer(player1);

        // not your turn
        result = controller.attack(player2.getClientId());
        assertEquals(Controller.notYourTurn, result);

        player1.setPosition(gameMap.getSector(new Point(5, 9)));
        player3.setPosition(gameMap.getSector(new Point(5, 9)));
        // move before attacking
        result = controller.attack(player1.getClientId());
        assertEquals(Controller.moveFirst, result);
        // cannot attack if already drawn a sector card
        player1.setDidMove(true);
        controller.draw(player1.getClientId());
        result = controller.attack(player1.getClientId());
        assertEquals(Controller.alreadyDrawn, result);
        // check if attack() works
        player1.resetFlags();
        player1.setDidMove(true);
        assertTrue(playersQueue.getPlayers().contains(player3));
        controller.attack(player1.getClientId());
        assertTrue(player1.getDidAttack());
        assertFalse(playersQueue.getPlayers().contains(player3));
        assertTrue(playersQueue.getRemovedPlayers().contains(player3));
        // cannot attack two times
        result = controller.attack(player1.getClientId());
        assertEquals(Controller.alreadyAttacked, result);

        // Humans cannot attack
        gameState.setCurrentPlayer(player3);
        player3.setDidMove(true);
        result = controller.attack(player3.getClientId());
        assertEquals(Controller.cantAttackHuman, result);

    }

    @Test
    public void testEndTurn() {
        String result;

        // Game not started yet
        result = controller.endTurn(UUID.randomUUID());
        assertEquals(Controller.gameNotStarted, result);

        gameState.setCurrentPlayer(player1);
        player1.setPosition(gameMap.getSector(new Point(5,9)));
        // not your turn
        result = controller.endTurn(player2.getClientId());
        assertEquals(Controller.notYourTurn, result);
        // cannot end turn before moving
        result = controller.endTurn(player1.getClientId());
        assertEquals(Controller.moveFirst, result);
        // cannot end turn before attacking or drawing
        player1.setDidMove(true);
        result = controller.endTurn(player1.getClientId());
        assertEquals(Controller.drawOrAttackFirst, result);
        // cannot end turn before making noise
        player1.setDidDraw(true);
        player1.setHaveToNoise(true);
        result = controller.endTurn(player1.getClientId());
        assertEquals(Controller.makeNoiseFirst, result);
        // cannot end turn with more than three items
        player1.setHaveToNoise(false);
        player1.addItem(new Spotlight());
        player1.addItem(new Attack());
        player1.addItem(new Defense());
        player1.addItem(new Adrenaline());
        result = controller.endTurn(player1.getClientId());
        assertEquals(4, player1.getItems().size());
        assertEquals(Controller.moreThanThreeItems, result);
        // check if turn is ended
        controller.discardItem(player1.getClientId(), player1.getClientId() + " discard spotlight");
        result = controller.endTurn(player1.getClientId());
        assertEquals(Controller.turnOver, result);
    }
    //
    // @Test
    // public void testChat() {
    // fail("Not yet implemented");
    // }
    //
    // @Test
    // public void testJoin() {
    // fail("Not yet implemented");
    // }

}
