package it.polimi.ingsw.cg_6.model.decks;

import static org.junit.Assert.assertEquals;
import it.polimi.ingsw.cg_6.server.model.cards.hatch.HatchCard;
import it.polimi.ingsw.cg_6.server.model.cards.hatch.HatchOpen;
import it.polimi.ingsw.cg_6.server.model.decks.HatchDeck;

import org.junit.Before;
import org.junit.Test;

public class HatchDeckTest {
    HatchDeck deck;
    HatchCard open;

    @Before
    public void init() {
        deck = new HatchDeck();
        open = new HatchOpen();
    }

    @Test
    public void testHatchDeck() {
        assertEquals(6, deck.getCards().size());
        assertEquals(true, (deck.getCards().get(5) instanceof HatchCard));
    }

}
