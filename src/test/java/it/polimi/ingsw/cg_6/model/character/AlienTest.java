package it.polimi.ingsw.cg_6.model.character;

import static org.junit.Assert.assertEquals;
import it.polimi.ingsw.cg_6.server.model.character.Alien;
import it.polimi.ingsw.cg_6.server.model.map.MapParser;
import it.polimi.ingsw.cg_6.server.model.move.Move;
import it.polimi.ingsw.cg_6.server.model.move.TwoSectorsMove;

import java.awt.Point;
import java.io.IOException;
import java.util.UUID;

import javax.xml.parsers.ParserConfigurationException;

import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

public class AlienTest {
    Alien alien;
    Move move;
    MapParser p = new MapParser();
    Point point = new Point(12, 6);
    UUID id = UUID.randomUUID();

    @Before
    public void init() throws IOException, SAXException,
            ParserConfigurationException {
        alien = new Alien(id);
        move = new TwoSectorsMove();
        p.createMap("Galilei");
        alien.setInitialPosition(p.getMap());
    }

    @Test
    public void testSetInitialPosition() {
        assertEquals(point, alien.getPosition().getPoint());
    }

    @Test
    public void testAlien() {
        assertEquals(move.getClass(), alien.getMove().getClass());
    }

    @Test
    public void testId() {
        assertEquals(id, alien.getClientId());
    }

}
