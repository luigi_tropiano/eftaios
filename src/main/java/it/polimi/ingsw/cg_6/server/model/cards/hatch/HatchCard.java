package it.polimi.ingsw.cg_6.server.model.cards.hatch;

import it.polimi.ingsw.cg_6.server.model.GameState;
import it.polimi.ingsw.cg_6.server.model.cards.Card;
import it.polimi.ingsw.cg_6.server.model.character.Player;
/**
 * the abstract class that represent the HatchCard
 * @author Daniele Parigi
 * @author Luigi Tropiano
 */
public abstract class HatchCard implements Card {
    /**
     * the activate of the card
     * @param player
     * @param gamestate
     * @return
     */
    public abstract String activate(Player player, GameState gamestate);

}