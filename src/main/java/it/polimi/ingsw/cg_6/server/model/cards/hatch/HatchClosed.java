package it.polimi.ingsw.cg_6.server.model.cards.hatch;

import it.polimi.ingsw.cg_6.client.InputConverter;
import it.polimi.ingsw.cg_6.server.model.GameState;
import it.polimi.ingsw.cg_6.server.model.character.Player;
import it.polimi.ingsw.cg_6.server.model.map.EscapeHatchSector;
/**
 * the card hatch close
 * @author Daniele Parigi
 * @author Luigi Tropiano
 */
public class HatchClosed extends HatchCard {
    /**
     * activate the card and updates log
     * @param player that draws this
     * @param gameState
     * @return "the escape hatch is broken"
     */
    @Override
    public String activate(Player player, GameState gameState) {
        ((EscapeHatchSector) player.getPosition()).setAvailable(false);
        gameState.addLog(player.toString()
                + " got to the escape hatch in sector "
                + InputConverter.intToChar(player.getPosition().getPoint().x)
                + " " + player.getPosition().getPoint().y
                + " but found it closed");
        return "the escape hatch is broken";
    }
}
