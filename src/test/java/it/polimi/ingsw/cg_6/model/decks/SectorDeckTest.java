package it.polimi.ingsw.cg_6.model.decks;

import static org.junit.Assert.assertEquals;

import java.util.UUID;

import it.polimi.ingsw.cg_6.server.model.cards.sector.SectorCard;
import it.polimi.ingsw.cg_6.server.model.cards.sector.Silence;
import it.polimi.ingsw.cg_6.server.model.character.Alien;
import it.polimi.ingsw.cg_6.server.model.character.Player;
import it.polimi.ingsw.cg_6.server.model.decks.SectorDeck;

import org.junit.Before;
import org.junit.Test;

public class SectorDeckTest {

    SectorDeck deck;
    Player alien;
    SectorCard silence;

    @Before
    public void init() {
        deck = new SectorDeck();
        alien = new Alien(UUID.randomUUID());
        silence = new Silence();
    }

    @Test
    public void testSectorDeck() {
        assertEquals(25, deck.getCards().size());
        assertEquals(true, deck.getCards().get(8) instanceof SectorCard);

    }

    @Test
    public void testDrawCard() {
        assertEquals(true, deck.drawCard() instanceof SectorCard);
    }

}
