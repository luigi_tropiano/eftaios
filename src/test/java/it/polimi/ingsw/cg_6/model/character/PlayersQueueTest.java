package it.polimi.ingsw.cg_6.model.character;

import static org.junit.Assert.assertEquals;
import it.polimi.ingsw.cg_6.server.Topic;
import it.polimi.ingsw.cg_6.server.model.character.Alien;
import it.polimi.ingsw.cg_6.server.model.character.Human;
import it.polimi.ingsw.cg_6.server.model.character.Player;
import it.polimi.ingsw.cg_6.server.model.character.PlayersQueue;

import java.util.UUID;

import org.junit.Before;
import org.junit.Test;

public class PlayersQueueTest {
    Player player1, player2;
    PlayersQueue coda;
    UUID idh = UUID.randomUUID();
    UUID ida = UUID.randomUUID();

    @Before
    public void init() {
        player1 = new Human(idh);
        player2 = new Alien(ida);
        coda = new PlayersQueue(new Topic());
        coda.addPlayer(player1);
        coda.addPlayer(player2);
    }

    @Test
    public void testAddPlayer() {
        assertEquals(player1, coda.getPlayers().get(0));
    }

    @Test
    public void testGetPlayers() {
        assertEquals(player2, coda.getPlayers().get(1));
    }

    @Test
    public void testNextPlayer() {
        assertEquals(player2, coda.nextPlayer(player1));
        assertEquals(player1, coda.nextPlayer(player2));
    }
}
