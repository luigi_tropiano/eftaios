package it.polimi.ingsw.cg_6.server.model.cards.item;

import it.polimi.ingsw.cg_6.server.model.GameState;
import it.polimi.ingsw.cg_6.server.model.cards.Card;
/**
 * abstract class to define the ItemCard type
 * @author Daniele Parigi
 * @author Luigi Tropiano
 */
public abstract class ItemCard implements Card {
    
    /**
     * what the card does if activated
     */
	public abstract String activate(GameState gameState);
	
	@Override
	public abstract String toString();
}