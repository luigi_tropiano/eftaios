package it.polimi.ingsw.cg_6.server.socket;

import it.polimi.ingsw.cg_6.server.controller.ServerTable;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;
import java.util.UUID;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BrokerThread extends Thread {
    /*
     * La nuova socket, verso uno specifico subscriber, creata dalla
     * ServerSocket
     */
    private Socket socket;
    private Logger logger = Logger.getLogger("escape");

    /*
     * Abbiamo soltanto bisogno di recapitare il messaggio. Il pattern non
     * prevede la ricezione, da parte del publisher, di alcun messaggio ( se non
     * la richiesta di sottoscrizione che tuttavia viene catturata dalla accept
     * nella ServerSocket)
     */
    private PrintWriter out;
    // private BufferedReader in;
    private Scanner in;
    private UUID id;

    // Una coda che contiene i messaggi, specifici per ciascun subscriber
    private ConcurrentLinkedQueue<String> buffer;

    /**
     * Quando un client esterno si sottoscrive viene creato un nuovo thread che
     * rappresenterá la specifica connessione allo specifico client/subscriber.
     * 
     * @param socket
     *            La nuova socket, verso uno specifico subscriber, creata dalla
     *            ServerSocket
     */
    public BrokerThread(Socket socket, ServerTable serverTable) {
        this.socket = socket;
        buffer = new ConcurrentLinkedQueue<String>();

        try {
            this.out = new PrintWriter(socket.getOutputStream());
            System.out.println("Adding new subscriber");
            this.in = new Scanner(socket.getInputStream());
            this.id = UUID.fromString(in.nextLine());
            serverTable.subscribe(id, this);
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Socket Broker Thread", e);
        }
    }

    /**
     * Questo metodo contiene, di fatto, la logica della comunicazione dal
     * publisher allo specifico subscriber.
     */
    @Override
    public void run() {
        while (true) {
            // Si prova ad estrarre un messaggio dalla coda...
            String msg = buffer.poll();
            // ... se c'é lo si invia
            if (msg != null)
                send(msg);
            else {
                // ... altrimenti, per evitare cicli inutili di CPU
                // che possono portare ad utilizzarla inutilmente...
                try {
                    // ... si aspetta fin quando la coda non conterrá qualcosa
                    // é necessario sincronizzarsi sull'oggetto monitorato, in
                    // modo tale
                    // che il thread corrente possieda il monitor sull'oggetto.
                    synchronized (buffer) {
                        buffer.wait();
                    }
                } catch (InterruptedException e) {
                    logger.log(Level.SEVERE, "Socket Broker Thread", e);
                }
            }
        }
    }

    /**
     * Questo metodo inserisce un messaggio nella coda e notifica ai thread in
     * attesa (in questo caso a se stesso) la presenza di un messaggio.
     * 
     * @param msg
     *            Il messaggio da inserire.
     */
    public void dispatchMessage(String msg) {
        buffer.add(msg);
        // é necessario sincronizzarsi sull'oggetto monitorato
        synchronized (buffer) {
            buffer.notify();
        }
    }

    /**
     * Questo metodo invia il messaggio al subscriber tramite la rete
     * 
     * @param msg
     */
    private void send(String msg) {
        out.println(msg);
        out.flush();
    }

    public void close() {
        try {
            socket.close();
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Socket Broker Thread", e);
        } finally {
            out = null;
            socket = null;
        }
    }

}