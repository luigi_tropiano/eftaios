package it.polimi.ingsw.cg_6.server.model.character;

import it.polimi.ingsw.cg_6.server.Topic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Handles all the players in a Game with their related status:
 * playing, pending or removed.
 * 
 * @author Luigi Tropiano
 * @author Daniele Parigi
 *
 */
public class PlayersQueue {

    private List<Player> players;
    private List<Player> removedPlayers;
    private List<Player> pendingPlayers;
    private Topic topic;

    public PlayersQueue(Topic topic) {
        this.topic = topic;
        this.players = new ArrayList<Player>();
        this.removedPlayers = new ArrayList<Player>();
        this.pendingPlayers = new ArrayList<Player>();
    }

    public void addPlayer(Player player) {
        players.add(player);
    }

    public List<Player> getPlayers() {
        return players;
    }

    public List<Player> getRemovedPlayers() {
        return removedPlayers;
    }

    public List<Player> getPendingPlayers() {
        return pendingPlayers;
    }

    public void shufflePlayers() {
        Collections.shuffle(players);
    }

    /**
     * @param player
     * 
     * @return the player in the playing players array just after the player
     *         passed. if the player passed is the last one of the array, it
     *         return the first one.
     */
    public Player nextPlayer(Player player) {
        if (players.indexOf(player) != players.size() - 1)
            return players.get(players.indexOf(player) + 1);
        else
            return players.get(0);
    }

    public Topic getTopic() {
        return topic;
    }

    /**
     * It removes the player from the playing players array and add it to the
     * removed players one.
     * 
     * @param player
     *            the player to be removed from playing players array and to be
     *            added to the removed players one.
     */
    public void removePlayer(Player player) {
        if (players.remove(player))
            removedPlayers.add(player);
    }

    /**
     * If the 'player' is in a pending state, it recovers it adding it back to
     * the playing players array.
     * 
     * @param player
     *            the pending player to be recovered.
     */
    public void recoverPlayer(Player player) {
        if (pendingPlayers.remove(player))
            players.add(player);
    }
}
