package it.polimi.ingsw.cg_6.server.model.map;

import java.awt.Point;

public class SecureSector extends Sector {

    public SecureSector(Point point) {
        setPoint(point);
    }
}
