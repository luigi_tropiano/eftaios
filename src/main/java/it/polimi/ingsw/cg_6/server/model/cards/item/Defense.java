package it.polimi.ingsw.cg_6.server.model.cards.item;

import it.polimi.ingsw.cg_6.server.model.GameState;

/**
 * the card Defense
 * 
 * @author Daniele Parigi
 * @author Luigi Tropiano
 *
 */
public class Defense extends ItemCard {
	@Override
    public String toString() {
	    return "defense";
    }
	/**
	 * you cannot use defense
	 * @return "you cannot use defense"
	 */
	@Override
    public String activate(GameState gameState) {
	    return "you cannot use defense";
    }
}
