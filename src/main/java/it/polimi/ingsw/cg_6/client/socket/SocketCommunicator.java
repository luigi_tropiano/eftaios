package it.polimi.ingsw.cg_6.client.socket;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * it is use both from Server and SocketClient, it creates socket connection and
 * it handles the methods to communicate through Socket
 * 
 * @author Luigi Tropiano
 * @author Daniele Parigi
 */
public class SocketCommunicator implements Communicator {

    private Socket socket;
    private Scanner in;
    private PrintWriter out;
    private Logger logger = Logger.getLogger("escape");

    public SocketCommunicator(Socket socket) {
        this.socket = socket;
        try {
            in = new Scanner(socket.getInputStream());
            out = new PrintWriter(socket.getOutputStream());
        } catch (IOException ex) {
            throw new AssertionError(
                    "some weird configuration problem occured or pebkac and you haven't opened the socket yet",
                    ex);
        }
    }

    @Override
    public void send(String msg) {
        out.println(msg);
        out.flush();
    }

    @Override
    public String receive() {
        return in.nextLine();
    }

    @Override
    public void close() {
        try {
            socket.close();
        } catch (IOException e) {
            logger.log(Level.SEVERE, "socket close", e);
        } finally {
            socket = null;
        }
    }
}