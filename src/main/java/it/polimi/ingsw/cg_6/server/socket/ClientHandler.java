package it.polimi.ingsw.cg_6.server.socket;

import it.polimi.ingsw.cg_6.client.socket.Communicator;
import it.polimi.ingsw.cg_6.server.controller.Controller;
import it.polimi.ingsw.cg_6.server.controller.ServerTable;

public class ClientHandler implements Runnable {
    
    Communicator client;
    ServerTable serverTable;

    
    public ClientHandler(Communicator client, ServerTable serverTable) {
        this.client = client;
        this.serverTable = serverTable;
    }
    
    @Override
    public void run(){
        String input = "";
        String[] split;

        do {
            input = client.receive();
            split = input.split(" ");
            Controller controller = new Controller(serverTable);
            client.send(controller.parser(input));
        } while(!split[1].equals("exit"));
        client.close();
    }

}