package it.polimi.ingsw.cg_6.server.model.cards.sector;

import it.polimi.ingsw.cg_6.server.model.GameState;
import it.polimi.ingsw.cg_6.server.model.cards.item.ItemCard;
import it.polimi.ingsw.cg_6.server.model.character.Player;

/**
 * the noise everywhere card with item attacked
 * 
 * @author Daniele Parigi
 * @author Luigi Tropiano
 */
public class NoiseEverywhereObj extends SectorCard {
    /**
     * activate of the noise everywhere card, draw an item and log the result
     * 
     * @param player
     * @param gameState
     * @return the request to make noise
     */
    @Override
    public String activate(Player player, GameState gameState) {
        ItemCard item = (ItemCard) gameState.getItemDeck().drawCard();
        player.addItem(item);
        gameState.addLog(player.toString() + " draws a sector card");
        gameState.addLog(player.toString() + " finds an item");
        player.setHaveToNoise(true);
        return "you found [" + item.toString() + "]. "
                + NoiseEverywhere.CHOOSENOISE;
    }
}
