package it.polimi.ingsw.cg_6.server.model;

import it.polimi.ingsw.cg_6.server.model.character.Alien;
import it.polimi.ingsw.cg_6.server.model.character.Human;
import it.polimi.ingsw.cg_6.server.model.character.Player;
import it.polimi.ingsw.cg_6.server.model.character.PlayersQueue;
import it.polimi.ingsw.cg_6.server.model.decks.HatchDeck;
import it.polimi.ingsw.cg_6.server.model.decks.ItemDeck;
import it.polimi.ingsw.cg_6.server.model.decks.SectorDeck;
import it.polimi.ingsw.cg_6.server.model.map.GameMap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * 
 * @author Luigi Tropiano
 * @author Daniele Parigi
 */
public class GameState {
    private Integer gameId;
    private GameMap gameMap;
    private PlayersQueue playerQueue;
    private Player currentPlayer;
    private ItemDeck itemDeck;
    private SectorDeck sectorDeck;
    private HatchDeck hatchDeck;
    private int turnsCounter; // contatore dei turni interi.
    private int singleTurnsCounter; // contatore dei singoli turni individuali
    private int numTurns;
    private int turnLength;
    private ArrayList<String> singleTurnsLog;
    private Map<Integer, ArrayList<String>> log;
    private Timer timer;
    private boolean gameOver;

    public GameState(GameMap gameMap, PlayersQueue playerQueue, int turnLength) {
        this.gameMap = gameMap;
        this.playerQueue = playerQueue;
        this.currentPlayer = playerQueue.getPlayers().get(0);
        this.itemDeck = new ItemDeck();
        this.sectorDeck = new SectorDeck();
        this.hatchDeck = new HatchDeck();
        this.singleTurnsLog = new ArrayList<String>();
        this.log = new HashMap<Integer, ArrayList<String>>();
        this.singleTurnsCounter = 1;
        this.turnLength = turnLength;
        this.numTurns = 39;
        this.turnsCounter = 1;
        startTimer();
    }

    public List<String> getLog(Integer turn) {
        return log.get(turn);
    }

    public List<String> getSingleTurnsLog() {
        return singleTurnsLog;
    }

    public void addLog(String string) {
        singleTurnsLog.add(string);
    }

    /**
     * Save the log of the current turn into the HashMap log
     * 
     * @param log
     */
    public void saveLog() {
        this.log.put(singleTurnsCounter, singleTurnsLog);
        incrementSingleTurnsCounter();
        resetTurnLog();
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public void setCurrentPlayer(Player currentPlayer) {
        this.currentPlayer = currentPlayer;
    }

    public void incrementSingleTurnsCounter() {
        singleTurnsCounter++;
    }

    public void incrementTurnsCounter() {
        turnsCounter++;
    }

    public ItemDeck getItemDeck() {
        return itemDeck;
    }

    public SectorDeck getSectorDeck() {
        return sectorDeck;
    }

    public HatchDeck getHatchDeck() {
        return hatchDeck;
    }

    public int getSingleTurnsCounter() {
        return singleTurnsCounter;
    }

    public Integer getGameId() {
        return gameId;
    }

    public void setGameId(Integer gameId) {
        this.gameId = gameId;
    }

    public PlayersQueue getPlayerQueue() {
        return playerQueue;
    }

    public GameMap getGameMap() {
        return gameMap;
    }

    private void resetTurnLog() {
        this.singleTurnsLog = new ArrayList<String>();
    }

    public int getTurnsCounter() {
        return turnsCounter;
    }

    public void setTurnsCounter(int turnsCounter) {
        this.turnsCounter = turnsCounter;
    }

    /**
     * Resets the timer every time it is called. Sends to all players the log of
     * the turn just ended and calls the saveLog() method. Resets the current
     * player flags and check if the game can continue; otherwise it calls the
     * endGame and set the winners; if the game can continue, it sets in
     * 'currentPlayer' the next player and starts the Timer.
     * 
     */
    public void endTurn() {

        resetTimer();
        playerQueue.getTopic().publish("------------------------");
        // Send to all players what happens in last turn
        for (String string : singleTurnsLog)
            playerQueue.getTopic().publish(string);
        // Save current turn log into the HashMap that contains all logs
        // and reset the currentLog
        saveLog();
        currentPlayer.resetFlags();
        incrementSingleTurnsCounter();
        if (!isThereAnyAlien())
            setLeftPlayersWinner();
        // if there are no more humans or aliens playing, the resting playing
        // players win.
        if (!isThereAnyHuman() || (playerQueue.getPlayers().size() == 1) ) {
            endGame();
        }
        // if there there are no more available turns, or if there are no more
        // available Escape Hatches, all the Aliens still playing win.
        else if ((turnsCounter > numTurns)
                || (!gameMap.isThereAnyAvailableHatch())) {
            setLeftAliensWinner();
            endGame();
        } else {
            setCurrentPlayer(playerQueue.nextPlayer(currentPlayer));
            if (currentPlayer == playerQueue.getPlayers().get(0))
                incrementTurnsCounter();
            playerQueue.getTopic().publish(
                    "now is " + currentPlayer.toString() + "'s turn");
            startTimer();
        }
    }

    /**
     * It sets 'gameOver' boolean to true, then put all players (both pending
     * and playing) in 'removedPlayers' array and tell them if they won or lost
     * the match.
     */
    public void endGame() {
        gameOver = true;
        // tolgo tutti i giocatori dalla playerQueue.players e li metto nella
        // playerQueue.removedPlayer
        while (!playerQueue.getPlayers().isEmpty())
            playerQueue.removePlayer(playerQueue.getPlayers().get(0));
        // stessa operazione con i giocatori 'sospesi' nella
        // playerQueue.pendingPlayers
        while (!playerQueue.getPendingPlayers().isEmpty())
            playerQueue.removePlayer(playerQueue.getPendingPlayers().get(0));

        playerQueue.getTopic().publish("this game is over");
        for (Player player : playerQueue.getRemovedPlayers()) {
            if (player.isWinner())
                playerQueue.getTopic().sendTo(player.getClientId(),
                        "Congratulation, you won this match");
            else
                playerQueue.getTopic().sendTo(player.getClientId(),
                        "You lost this match");
        }
    }

    public boolean isGameOver() {
        return gameOver;
    }

    public void setGameOver(boolean gameOver) {
        this.gameOver = gameOver;
    }

    public void startTimer() {
        timer = new Timer();
        timer.schedule(new SkipPlayer(), turnLength * 1000);
    }

    public void resetTimer() {
        timer.cancel();
    }

    /**
     * @return true if there is some human in the current players playing array,
     *         else return false.
     */
    public boolean isThereAnyHuman() {
        for (Player player : playerQueue.getPlayers())
            if (player instanceof Human)
                return true;
        return false;
    }

    /**
     * @return true if there is some alien in the current players playing array,
     *         else return false.
     */
    public boolean isThereAnyAlien() {
        for (Player player : playerQueue.getPlayers())
            if (player instanceof Alien)
                return true;
        return false;
    }

    /**
     * Sets all the players still playing to winner.
     */
    private void setLeftPlayersWinner() {
        for (Player player : playerQueue.getPlayers())
            player.setWinner(true);
    }

    /**
     * Sets all the players still playing to winner.
     */
    private void setLeftAliensWinner() {
        for (Player player : playerQueue.getPlayers())
            if (player instanceof Alien)
                player.setWinner(true);
    }

    /**
     * Contains the task that the timer executes after its countdown.
     * 
     * @author Luigi Tropiano
     * @author Daniele Parigi
     */
    class SkipPlayer extends TimerTask {
        /*
         * It removes the current player from playing players array and add him
         * to the pendingPlayers one; then it calls the endTurn() method.
         */
        @Override
        public void run() {
            playerQueue.getPlayers().remove(currentPlayer);
            playerQueue.getPendingPlayers().add(currentPlayer);
            addLog("Player " + currentPlayer.toString()
                    + " has been suspended from the game for timeout");
            endTurn();
        }
    }
}