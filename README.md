# README #

Project of Ingegneria del Software by Luigi Trpoiano & Daniele Parigi 

### How to run our project ###

* First run the Server class (package it.polimi.ingsw.cg_6.server)
* Then run one of the available Clients, the CLI one (package it.polimi.ingsw.cg_6.client.CLI) or the GUI one (package it.polimi.ingsw.cg_6.client.GUI)
* Both the clients will ask you for an IP address and the connection method you prefer (if socket or RMI)
* Play the game :)

### How it works ###

* Right after the client starts up, it will join the first available room on the server.
* The game starts when the room gets 8 players or after a countdown (default is 15 seconds).
* Each player has 3 minutes to complete his turn, otherwise he is suspended from the game.
* If a suspended player send a command, he will be added back to the game.

### How to play from CLI ###

### Available commands are: ###

* move <letter> <number>
* noise <letter> <number>
* use <item>
* use spotlight <letter> <number>
* discard <item>
* attack
* draw
* chat <message>
* end

### e.g. ###
* move k 9
* use spotlight l 10
* discard defense

### How to play from GUI ###

* To move into some sector just click that sector on the map.
* When the server tells you to choose where to make noise just click the sector you prefer on the map.
* Action buttons are located in the bottom right area of the frame. To attack press the red one, to draw a card press the blue one, to end the turn press the green one and to discard an item press the black button with a box painted on.
* To use an item click one of the buttons just above the Action ones. To use spotlight you have to click a sector on the map after clicking the spotlight button.
* To write a chat message just use the input area on the left of the 'send' button.

### Other info ###

* In the res/ folder there are all the three maps of the official game in .xml format. Both server and clients's GUI parse the map through the MapParser class.
* To implement a new map just add a new xml file in the res/ folder following the model of the already existing maps.

The GUI has a rare glitch, just click on a sector to solve it