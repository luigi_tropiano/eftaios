package it.polimi.ingsw.cg_6.model.map;

import static org.junit.Assert.*;
import it.polimi.ingsw.cg_6.server.model.map.DangerousSector;
import it.polimi.ingsw.cg_6.server.model.map.GameMap;
import it.polimi.ingsw.cg_6.server.model.map.MapParser;
import it.polimi.ingsw.cg_6.server.model.map.Sector;

import java.awt.Point;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.junit.Test;
import org.xml.sax.SAXException;

public class MapParserTest {

    MapParser mp = new MapParser();
    String string = "Galilei";
    GameMap map = new GameMap();
    Point point = new Point(3, 7);
    Sector secure = new DangerousSector(point);

    @Test
    public void testCreateMap() throws IOException, SAXException,
            ParserConfigurationException {
        mp.createMap(string);
        assertEquals(false, mp.getMap().getSectors().isEmpty());
        assertEquals(secure.getClass(), mp.getMap().getSector(point).getClass());
        assertEquals(false, mp.getMap().getSector(point).getNearSectors()
                .isEmpty());
    }

    @Test
    public void testAddSector() {
        map.addSector(point, secure);
        assertEquals(1, map.getSectors().size());
    }

    @Test
    public void testAddNearSector() {
        assertEquals(3, mp.getMap().getSector(point).getNearSectors().size());
    }

    @Test
    public void AlienAndHumanTest() throws IOException, SAXException,
            ParserConfigurationException {
        mp.createMap(string);
        for (Sector s : mp.getMap().getSector(point).getNearSectors())
            System.out.println(s.getPoint());
        assertEquals(12, mp.getMap().getAlienSector().getPoint().x);
        assertEquals(6, mp.getMap().getAlienSector().getPoint().y);
    }
}
