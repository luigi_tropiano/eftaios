package it.polimi.ingsw.cg_6.client;

/**
 * @author Luigi Tropiano
 * @author Daniele Parigi
 */
public class InputConverter {

    private final static String alphabet = "#ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    private InputConverter(){
    }

    /**
     * @param input
     *            the char to convert
     * @return the integer related to char in input: 'A' and 'a' returns '1',
     *         'Z' and 'z' return '26'
     * 
     */
    public static int charToInt(char input) {
        if (!Character.isUpperCase(input))
            input = Character.toUpperCase(input);
        return alphabet.indexOf(input);
    }

    /**
     * @param input
     *            the int to convert
     * @return the char related to the integer in input: '1' returns 'A', '26'
     *         returns 'Z', an input greater than '26' or lower than '0' return
     *         '#'
     */
    public static char intToChar(int input) {
        if (input < 0 || input > 26)
            return '#';
        return alphabet.charAt(input);
    }
}
