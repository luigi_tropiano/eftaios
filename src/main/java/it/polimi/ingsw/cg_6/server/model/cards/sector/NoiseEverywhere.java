package it.polimi.ingsw.cg_6.server.model.cards.sector;

import it.polimi.ingsw.cg_6.server.model.GameState;
import it.polimi.ingsw.cg_6.server.model.character.Player;
/**
 * the noise everywhere card
 * 
 * @author Daniele Parigi
 * @author Luigi Tropiano
 */
public class NoiseEverywhere extends SectorCard {
    
    public static final String CHOOSENOISE = "choose where you want to make noise";
    /**
     * activate of the noise everywhere card and log the result
     * 
     * @param player
     * @param gameState
     * @return the request to make noise
     */
    @Override
    public String activate(Player player, GameState gameState) {
        gameState.addLog(player.toString() + " draws a sector card");
        player.setHaveToNoise(true);
        return CHOOSENOISE;
    }

}
