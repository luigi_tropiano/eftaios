package it.polimi.ingsw.cg_6.model.move;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import it.polimi.ingsw.cg_6.server.model.character.Alien;
import it.polimi.ingsw.cg_6.server.model.character.Human;
import it.polimi.ingsw.cg_6.server.model.map.MapParser;
import it.polimi.ingsw.cg_6.server.model.move.ThreeSectorsMove;

import java.awt.Point;
import java.io.IOException;
import java.util.UUID;

import javax.xml.parsers.ParserConfigurationException;

import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

public class MoveTest {
    
    private Human human;
    private Alien alien;
    private UUID clientId;
    MapParser mp;
    
    @Before
    public void init() throws IOException, SAXException, ParserConfigurationException{
        clientId = UUID.randomUUID();
        human = new Human(clientId);
        alien = new Alien(clientId);
        mp = new MapParser();
        mp.createMap("Galilei");
        human.setInitialPosition(mp.getMap());
        alien.setInitialPosition(mp.getMap());
    }

    @Test
    public void humanTest() {
        assertTrue(human.getMove().isValid(human, mp.getMap().getSector(new Point(11, 8))));
        assertFalse(human.getMove().isValid(human, mp.getMap().getSector(new Point(2, 8))));
    }
    
    @Test
    public void alienTest() {
        assertTrue(alien.getMove().isValid(alien, mp.getMap().getSector(new Point(12, 4))));
        assertFalse(alien.getMove().isValid(alien, mp.getMap().getSector(new Point(12, 10))));
    }
    
    @Test
    public void threeSectorMoveTest() {
        alien.setMove(new ThreeSectorsMove());
        assertTrue(alien.getMove().isValid(alien, mp.getMap().getSector(new Point(9, 5))));
        assertTrue(alien.getMove().isValid(alien, mp.getMap().getSector(new Point(12, 5))));
        assertFalse(alien.getMove().isValid(alien, mp.getMap().getSector(new Point(7, 5))));
    }
}
