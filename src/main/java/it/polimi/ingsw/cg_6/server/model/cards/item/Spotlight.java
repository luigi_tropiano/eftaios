package it.polimi.ingsw.cg_6.server.model.cards.item;

import it.polimi.ingsw.cg_6.client.InputConverter;
import it.polimi.ingsw.cg_6.server.model.GameState;
import it.polimi.ingsw.cg_6.server.model.character.Player;
import it.polimi.ingsw.cg_6.server.model.map.Sector;

import java.awt.Point;
import java.util.ArrayList;

/**
 * the card Adrenaline, to use this, instead of activate(GameState) use
 * rightActivate(GameState, String)
 * 
 * @author Daniele Parigi
 * @author Luigi Tropiano
 *
 */
public class Spotlight extends ItemCard {

    private static final String SPOTTED = " is spotted in sector ";

    @Override
    public String toString() {
        return "spotlight";
    }

    /**
     * check the presence of players in the pointed by line sector and in the
     * near sector, updates the log, discards the card and removes it from the
     * player hand
     * 
     * @param gameState
     *            the game to search in
     * @param line
     *            to get the sector to spotlight
     * @return The String to describe action performed
     */
    public String rightActivate(GameState gameState, String line) {
        String[] split = line.split(" ");
        ArrayList<String> playersSpotted = new ArrayList<String>();
        Sector sector = gameState.getGameMap().getSector(
                new Point(Integer.parseInt(split[3]), Integer
                        .parseInt(split[4])));
        gameState.addLog(gameState.getCurrentPlayer().toString()
                + " uses item [" + this.toString() + "]");
        gameState.getItemDeck().discardCard(this);
        gameState.getCurrentPlayer().getItems().remove(this);
        // search the player position
        for (Player enlightened : gameState.getPlayerQueue().getPlayers()) {
            if (enlightened.getPosition() == sector) {
                playersSpotted.add(enlightened.toString() + SPOTTED + " "
                        + InputConverter.intToChar(sector.getPoint().x) + " "
                        + sector.getPoint().y);
            }
            // search the near sector
            for (Sector nearSector : sector.getNearSectors())
                if (enlightened.getPosition() == nearSector) {
                    playersSpotted.add(enlightened.toString() + SPOTTED + " "
                            + InputConverter.intToChar(nearSector.getPoint().x)
                            + " " + nearSector.getPoint().y);
                }
        }
        if (playersSpotted.isEmpty())
            return "you didn't find anyone";

        String string = "";
        for (String s : playersSpotted) {
            gameState.addLog(s);
            string = string + s + ", ";
        }
        // remove last ", "
        string = string.substring(0, string.length() - 2);
        return string;
    }

    /**
     * Don't call this use rightActivate(GameState, String)
     */
    @Override
    public String activate(GameState gameState) {
        return "something strange happens";
    }
}