package it.polimi.ingsw.cg_6.server.model.map;

import java.awt.Point;
import java.util.HashSet;
import java.util.Set;

public abstract class Sector {
    private Point point;

    private Set<Sector> nearSectors = new HashSet<Sector>();

    public Point getPoint() {
        return point;
    }

    public void setPoint(Point point) {
        this.point = point;
    }

    public Set<Sector> getNearSectors() {
        return nearSectors;
    }

    public void addNearSector(Sector sector) {
        if (sector != null)
            nearSectors.add(sector);
    }
}
