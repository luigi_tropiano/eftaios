package it.polimi.ingsw.cg_6.model.decks;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import it.polimi.ingsw.cg_6.server.model.cards.item.Attack;
import it.polimi.ingsw.cg_6.server.model.cards.item.ItemCard;
import it.polimi.ingsw.cg_6.server.model.character.Human;
import it.polimi.ingsw.cg_6.server.model.character.Player;
import it.polimi.ingsw.cg_6.server.model.decks.ItemDeck;

import java.util.UUID;

import org.junit.Before;
import org.junit.Test;

public class ItemDeckTest {

    ItemDeck deck;
    Player human;
    ItemCard attack;

    @Before
    public void init() {
        deck = new ItemDeck();
        human = new Human(UUID.randomUUID());
        attack = new Attack();
    }

    @Test
    public void testItemDeck() {
        assertEquals(12, deck.getCards().size());
        assertEquals(true, (deck.getCards().get(5) instanceof ItemCard));
    }

    @Test
    public void testDrawCard() {
        human.addItem(deck.drawCard());
        assertEquals(11, deck.getCards().size());
        assertEquals(1, human.getItems().size());
        assertTrue(human.getItems().get(0) instanceof ItemCard);
    }

    @Test
    public void testDiscardCard() {
        deck.discardCard(attack);
        assertEquals(false, deck.getDiscardedCards().isEmpty());
    }

    @Test
    public void testResetDeck() {
        deck.discardCard(attack);
        for (int i = 12; i > 0; i--)
            human.addItem(deck.drawCard());
        assertEquals(attack, deck.getCards().get(0));
        assertEquals(true, deck.getDiscardedCards().isEmpty());
    }

}
