package it.polimi.ingsw.cg_6.server.model.decks;

import it.polimi.ingsw.cg_6.server.model.cards.item.Adrenaline;
import it.polimi.ingsw.cg_6.server.model.cards.item.Attack;
import it.polimi.ingsw.cg_6.server.model.cards.item.Defense;
import it.polimi.ingsw.cg_6.server.model.cards.item.Sedatives;
import it.polimi.ingsw.cg_6.server.model.cards.item.Spotlight;
import it.polimi.ingsw.cg_6.server.model.cards.item.Teleport;

public class ItemDeck extends Deck {

    public ItemDeck() {

        for (int i = 2; i > 0; i--) {
            addCard(new Attack());
            addCard(new Spotlight());
            addCard(new Teleport());
            addCard(new Adrenaline());
        }
        for (int i = 3; i > 0; i--) {
            addCard(new Sedatives());
        }
        addCard(new Defense());

        shuffleDeck();
    }

}
