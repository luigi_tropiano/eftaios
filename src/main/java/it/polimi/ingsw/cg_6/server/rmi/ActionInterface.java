package it.polimi.ingsw.cg_6.server.rmi;

import java.awt.Point;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.UUID;

public interface ActionInterface extends Remote{

    public String use(UUID id, String s) throws RemoteException;
    public String discardItem(UUID id, String s) throws RemoteException;
    public String join(UUID id) throws RemoteException;
    public String move(UUID id, Point p) throws RemoteException;
    public String draw(UUID id) throws RemoteException;
    public String attack(UUID id) throws RemoteException;
    public String chat(UUID id, String s) throws RemoteException;
    public String noise(UUID id, Point p) throws RemoteException;
    public String endTurn(UUID id) throws RemoteException;
}
