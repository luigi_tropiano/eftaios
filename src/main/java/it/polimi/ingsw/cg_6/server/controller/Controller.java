package it.polimi.ingsw.cg_6.server.controller;

import it.polimi.ingsw.cg_6.client.InputConverter;
import it.polimi.ingsw.cg_6.server.model.GameState;
import it.polimi.ingsw.cg_6.server.model.cards.Card;
import it.polimi.ingsw.cg_6.server.model.cards.hatch.HatchCard;
import it.polimi.ingsw.cg_6.server.model.cards.item.Attack;
import it.polimi.ingsw.cg_6.server.model.cards.item.ItemCard;
import it.polimi.ingsw.cg_6.server.model.cards.item.Spotlight;
import it.polimi.ingsw.cg_6.server.model.cards.sector.SectorCard;
import it.polimi.ingsw.cg_6.server.model.character.Alien;
import it.polimi.ingsw.cg_6.server.model.character.Player;
import it.polimi.ingsw.cg_6.server.model.map.DangerousSector;
import it.polimi.ingsw.cg_6.server.model.map.EscapeHatchSector;
import it.polimi.ingsw.cg_6.server.model.map.SecureSector;
import it.polimi.ingsw.cg_6.server.model.move.ThreeSectorsMove;
import it.polimi.ingsw.cg_6.server.rmi.ActionInterface;

import java.awt.Point;
import java.util.UUID;

/**
 * contains the method to elaborate String from the client and make the actions
 * requested
 * 
 * @author Daniele Parigi
 * @author Luigi Tropiano
 *
 */
public class Controller implements ActionInterface {

    private ServerTable serverTable;
    public static final String alreadyDrawn = "you have already drawn";
    public static final String alreadyMoved = "you have already moved";
    public static final String alreadyAttacked = "you have already attacked, enough meat for today";
    public static final String gameNotStarted = "game hasn't started yet";
    public static final String cantDrawHere = "you can't draw here";
    public static final String cantMoveThere = "you can't move there";
    public static final String cantAttackHuman = "you cannot attack, useless human";
    public static final String notYourTurn = "it's not your turn";
    public static final String notThatItem = "you do not have that item";
    public static final String moveFirst = "you have to move first";
    public static final String makeNoiseFirst = "you have to make noise first";
    public static final String drawOrAttackFirst = "you have to draw or attack first";
    public static final String noNeedToDraw = "you attacked, no need to draw";
    public static final String noNeedToDiscard = "you don't have three objects yet, no need to discard";
    public static final String noItemsForYou = "you are an alien, no items for you";
    public static final String turnOver = "your turn is over";
    public static final String moreThanThreeItems = "it seems you have more than 3 items;"
            + " you have to [use] or [discard] one of them before ending your turn";
    public static final String pendingPlayer = "it seems you have been suspended from the game for timeout but don't worry, I'm adding you back";

    public Controller(ServerTable serverTable) {
        this.serverTable = serverTable;
    }

    /**
     * pars the inLine received by client and return the responses
     * 
     * @param inLine
     *            to pars
     * @return the String to send back to client
     */
    public String parser(String inLine) {
        // only used by socket
        String[] split;
        split = inLine.split(" ");
        UUID id = UUID.fromString(split[0]);
        switch (split[1]) {
        case "join":
            return join(id);
        case "chat":
            return chat(id, inLine);
        case "move":
            return move(
                    id,
                    new Point(Integer.parseInt(split[2]), Integer
                            .parseInt(split[3])));
        case "draw":
            return draw(id);
        case "attack":
            return attack(id);
        case "noise":
            return noise(
                    id,
                    new Point(Integer.parseInt(split[2]), Integer
                            .parseInt(split[3])));
        case "end":
            return endTurn(id);
        case "use":
            return use(id, inLine);
        case "discard":
            return discardItem(id, inLine);
        default:
            return "command refused";
        }
    }

    /**
     * uses an item checking the EFTAIOS rules, it uses the line to check which
     * item to use, check if the player has the item and call the card
     * activate() to use the item discard the item if successfully used
     * 
     * @param id
     *            the client ID
     * @param line
     *            the parsed string received by server
     * @return String that described the action performed
     */
    @Override
    public String use(UUID id, String line) {
        GameState gameState = getGameState(id);
        if (gameState == null)
            return gameNotStarted;
        Player player = getPlayer(id);
        if (!isPlayerTurn(gameState, player))
            if (isPlayerPending(gameState, player))
                return pendingPlayer;
            else
                return notYourTurn;
        if (player instanceof Alien)
            return noItemsForYou;
        String[] split = line.split(" ");
        for (Card item : player.getItems()) {
            // attack doesn't call the card activate() but uses the existing
            // method attack()
            if (item.toString().equalsIgnoreCase(split[2])
                    && item instanceof Attack) {
                player.setCanAttack(true);
                String attack = attack(id);
                if (player.getDidAttack()) {
                    gameState.getItemDeck().discardCard(item);
                    player.getItems().remove(item);
                }
                return attack;
                // spotlight need the coordinates to work
            } else if (item.toString().equalsIgnoreCase(split[2])
                    && item instanceof Spotlight) {
                return ((Spotlight) item).rightActivate(gameState, line);
            } else if (item.toString().equalsIgnoreCase(split[2])) {
                return ((ItemCard) item).activate(gameState);
            }
        }
        return notThatItem;
    }

    /**
     * discard an item from the players hand checking the EFATIOS rules
     * 
     * @param id
     *            the client ID
     * @param line
     *            the parsed string received by server
     * @return String that described the action performed
     */
    @Override
    public String discardItem(UUID id, String line) {
        GameState gameState = getGameState(id);
        if (gameState == null)
            return gameNotStarted;
        Player player = getPlayer(id);
        if (!isPlayerTurn(gameState, player))
            if (isPlayerPending(gameState, player))
                return pendingPlayer;
            else
                return notYourTurn;
        if (player.getItems().size() < 3)
            return noNeedToDiscard;
        String[] split = line.split(" ");
        for (Card item : player.getItems()) {
            if (item.toString().equalsIgnoreCase(split[2])) {
                gameState.getItemDeck().discardCard(item);
                player.getItems().remove(item);
                return "[" + item.toString() + "] succesfully discarded";
            }
        }
        return notThatItem;

    }

    /**
     * the client join the available room
     * 
     * @param id
     *            the client UUID
     * @return String that described the action performed
     */
    @Override
    public String join(UUID id) {
        serverTable.getRoomList().joinRoom(id);
        return ("joining room...");
    }

    /**
     * move the player to the point checking the EFTAIOS rules, set a flag for
     * prevent double move by the player
     * 
     * @param id
     *            the client ID to move
     * @param point
     *            where to move the player
     * @return String that described the action performed
     */
    @Override
    public String move(UUID id, Point point) {
        GameState gameState = getGameState(id);
        if (gameState == null)
            return gameNotStarted;
        Player player = getPlayer(id);
        if (!isPlayerTurn(gameState, player))
            if (isPlayerPending(gameState, player))
                return pendingPlayer;
            else
                return notYourTurn;
        if (player.getDidMove())
            return alreadyMoved;
        else if (player.getMove().isValid(player,
                gameState.getGameMap().getSector(point))) {
            player.setDidMove(true);
            if (gameState.getGameMap().getSector(point) instanceof SecureSector)
                gameState.addLog(player.toString() + " is in a secure sector");
            return player.setPosition(gameState.getGameMap().getSector(point));
        } else
            return cantMoveThere;
    }

    /**
     * permits to draw sector card or hatch card related to the player position
     * (player in Dangerous Sector draw a sector card, player in Escape Hatch
     * draw hatch card) checking the EFATIOS rules
     * 
     * @param id
     *            the client UUID
     * @return String that described the action performed
     */
    @Override
    public String draw(UUID id) {
        GameState gameState = getGameState(id);
        if (gameState == null)
            return gameNotStarted;
        Player player = getPlayer(id);
        if (!isPlayerTurn(gameState, player))
            if (isPlayerPending(gameState, player))
                return pendingPlayer;
            else
                return notYourTurn;
        if (!player.getDidMove())
            return moveFirst;
        else if (player.getDidDraw())
            return alreadyDrawn;
        else if (player.getDidAttack())
            return noNeedToDraw;
        else {
            // these {else if} check the player position
            if (player.getPosition() instanceof DangerousSector) {
                player.setDidDraw(true);
                SectorCard card = (SectorCard) gameState.getSectorDeck()
                        .drawCard();
                gameState.getSectorDeck().discardCard(card);
                return card.activate(player, gameState);
            } else if (player.getPosition() instanceof EscapeHatchSector) {
                player.setDidDraw(true);
                HatchCard card = (HatchCard) gameState.getHatchDeck()
                        .drawCard();
                gameState.getHatchDeck().discardCard(card);
                return ((HatchCard) gameState.getHatchDeck().drawCard())
                        .activate(player, gameState);
            } else
                return cantDrawHere;
        }
    }

    /**
     * permits to make noise in pointed sector if the EFTAIOS rules permits it
     * 
     * @param id
     *            the client ID to move
     * @param point
     *            where to noise
     * @return String that described the action performed
     */
    @Override
    public String noise(UUID id, Point point) {
        GameState gameState = getGameState(id);
        if (gameState == null)
            return gameNotStarted;
        Player player = getPlayer(id);
        if (!isPlayerTurn(gameState, player))
            if (isPlayerPending(gameState, player))
                return pendingPlayer;
            else
                return notYourTurn;
        if (player.getHaveToNoise()) {
            player.setHaveToNoise(false);
            gameState.addLog(player.toString() + " makes noise in sector "
                    + InputConverter.intToChar(point.x) + " " + point.y);
            return "you made noise in sector "
                    + InputConverter.intToChar(point.x) + " " + point.y;
        } else
            return "in order to make noise in a sector you have to draw a noise everywhere card";
    }

    /**
     * permits to attack in player position if the EFTAIOS rules permits it
     * 
     * @param id
     *            the client ID attacking
     * @return String that described the action performed
     */
    @Override
    public String attack(UUID id) {
        GameState gameState = getGameState(id);
        if (gameState == null)
            return gameNotStarted;
        Player player = getPlayer(id);
        if (!isPlayerTurn(gameState, player))
            if (isPlayerPending(gameState, player))
                return pendingPlayer;
            else
                return notYourTurn;
        if (!player.getCanAttack())
            return cantAttackHuman;
        else if (!player.getDidMove())
            return moveFirst;
        else if (player.getDidAttack())
            return alreadyAttacked;
        else if (player.getDidDraw())
            return alreadyDrawn;
        else {
            player.setDidAttack(true);
            // updates the log
            gameState.addLog(player.toString()
                    + " attacks sector "
                    + InputConverter
                            .intToChar(player.getPosition().getPoint().x) + " "
                    + player.getPosition().getPoint().y);
            // line to pick the performed actions
            String line = "";
            int size = gameState.getPlayerQueue().getPlayers().size();
            // loop that search for player in the attacked sector
            while (size > 0) {
                Player dead = gameState.getPlayerQueue().getPlayers()
                        .get(size - 1);
                // if the player has been found
                if (dead.getPosition() == player.getPosition()
                        && dead != player) {
                    if (dead instanceof Alien) {
                        dead.kill(gameState);
                        line = line + "you kill the alien " + dead.toString()
                                + ", ";
                    } else {
                        if (dead.haveDefense(gameState.getItemDeck())) {
                            // updates log
                            gameState.addLog("the human " + dead.toString()
                                    + " defends himself from "
                                    + player.toString() + " attack");
                            line = line
                                    + "the human "
                                    + dead.toString()
                                    + " defends himself using the item [defense], ";
                        } else {
                            // dead is finally dead no control can save him
                            // anymore
                            dead.kill(gameState);
                            line = line + "you kill the human "
                                    + dead.toString() + ", ";
                            if (player instanceof Alien)
                                player.setMove(new ThreeSectorsMove());
                        }
                    }
                }
                size--;
            }
            if (line.isEmpty())
                return "no one to kill here";
            // remove last ", "
            line = line.substring(0, line.length() - 2);
            return line;
        }
    }

    /**
     * permits to end the player turn if the EFTAIOS rules permits it, if all
     * the controls pass launch gameState.endTurn() to update the players
     * 
     * @param id
     *            the client ID attacking
     * @return String that described the action performed
     */
    @Override
    public String endTurn(UUID id) {
        GameState gameState = getGameState(id);
        if (gameState == null)
            return gameNotStarted;
        Player player = getPlayer(id);
        if (!isPlayerTurn(gameState, player))
            if (isPlayerPending(gameState, player))
                return pendingPlayer;
            else
                return notYourTurn;
        if (!player.getDidMove())
            return moveFirst;
        // check if player have to draw or attack
        else if ((player.getPosition() instanceof DangerousSector || player
                .getPosition() instanceof EscapeHatchSector)
                && (!player.getDidDraw() && !player.getDidAttack()))
            return drawOrAttackFirst;
        else if (player.getHaveToNoise())
            return makeNoiseFirst;
        else if (player.getItems().size() > 3)
            return moreThanThreeItems;
        else {
            gameState.endTurn();
            return turnOver;
        }
    }

    /**
     * send the chat message to other player through the Topic(Pub-Sub)
     * 
     * @param id
     *            of the player sending the message
     * @param inLine
     *            to send to player after cleaning
     * @return "message sent" if sending success
     */
    @Override
    public String chat(UUID id, String inLine) {
        Player player = getPlayer(id);
        String line = inLine.substring(41);
        if (serverTable.getClientPlace().get(id) instanceof WaitingRoom) {
            return ((WaitingRoom) serverTable.getClientPlace().get(id))
                    .getTopic().publish("[" + id + "] " + line);
        } else
            return ((GameState) serverTable.getClientPlace().get(id))
                    .getPlayerQueue().getTopic()
                    .publish(player.toString() + " " + line);
    }

    /**
     * @param id
     *            the client id
     * @return the player related to the id
     */
    private Player getPlayer(UUID id) {
        return serverTable.getClientPlayerTable().get(id);
    }

    /**
     * 
     * @param id
     *            the client id
     * @return the GameState in which client plays. null if the game hasn't
     *         started yet
     * 
     */
    private GameState getGameState(UUID id) {
        if (serverTable.locate(id) instanceof GameState)
            return (GameState) serverTable.locate(id);
        else
            return null;
    }

    private boolean isPlayerTurn(GameState gameState, Player player) {
        return gameState.getCurrentPlayer() == player;
    }

    /**
     * control that permits to a disconnected player to restart playing
     * 
     * @param gameState
     * @param player
     * @return true if the player was disconnected, false if not
     */
    private boolean isPlayerPending(GameState gameState, Player player) {
        if (gameState.getPlayerQueue().getPendingPlayers().contains(player)) {
            gameState.getPlayerQueue().recoverPlayer(player);
            return true;
        }
        return false;
    }
}
