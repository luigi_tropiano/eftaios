package it.polimi.ingsw.cg_6.server.model.cards.item;

import it.polimi.ingsw.cg_6.server.model.GameState;
/**
 * the card Sedatives
 * 
 * @author Daniele Parigi
 * @author Luigi Tropiano
 *
 */
public class Sedatives extends ItemCard {
    @Override
    public String toString() {
        return "sedatives";
    }
    /**
     * permits the player to not draw, updates the log, discards the
     * card and removes it from the player hand
     * @param gameState
     * @return The String to describe action performed
     */
    @Override
    public String activate(GameState gameState) {
        gameState.getCurrentPlayer().setDidDraw(true);
        gameState.addLog(gameState.getCurrentPlayer().toString()
                + " use item [" + this.toString() + "]");
        gameState.getItemDeck().discardCard(this);
        gameState.getCurrentPlayer().getItems().remove(this);
        return "you activate stealth mode";
    }
}
