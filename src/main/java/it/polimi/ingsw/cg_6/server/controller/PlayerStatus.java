package it.polimi.ingsw.cg_6.server.controller;

public enum PlayerStatus {
    OUTROOM, INROOM, INGAME
}
