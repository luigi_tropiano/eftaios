package it.polimi.ingsw.cg_6.server.model.cards.sector;

import it.polimi.ingsw.cg_6.server.model.GameState;
import it.polimi.ingsw.cg_6.server.model.cards.Card;
import it.polimi.ingsw.cg_6.server.model.character.Player;

/**
 * the abstract class that represent the sectors
 * 
 * @author Daniele Parigi
 * @author Luigi Tropiano
 */
public abstract class SectorCard implements Card {
    /**
     * the activate of the sector
     * @param player
     * @param gameState
     * @return
     */
    public abstract String activate(Player player, GameState gameState);

}
