package it.polimi.ingsw.cg_6.server.controller;

import it.polimi.ingsw.cg_6.server.Topic;
import it.polimi.ingsw.cg_6.server.model.GameState;
import it.polimi.ingsw.cg_6.server.model.character.Alien;
import it.polimi.ingsw.cg_6.server.model.character.Human;
import it.polimi.ingsw.cg_6.server.model.character.NickName;
import it.polimi.ingsw.cg_6.server.model.character.Player;
import it.polimi.ingsw.cg_6.server.model.character.PlayersQueue;
import it.polimi.ingsw.cg_6.server.model.map.GameMap;
import it.polimi.ingsw.cg_6.server.model.map.MapParser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

/**
 * The Room where the clients wait for the game to start
 * 
 * @author Luigi Tropiano
 * @author Daniele Parigi
 *
 */
public class WaitingRoom {

    private Integer roomId;
    private String map;
    private int maxPlayers;
    private ArrayList<UUID> players;
    private Topic topic;
    private ServerTable serverTable;
    private Logger logger = Logger.getLogger("escape");
    private boolean isStarted;
    private int turnLength;

    public WaitingRoom(Integer roomId, String map, int maxPlayers, Topic topic,
            ServerTable serverTable) {
        this.roomId = roomId;
        this.map = map;
        this.maxPlayers = maxPlayers;
        this.players = new ArrayList<UUID>();
        this.topic = topic;
        this.serverTable = serverTable;
        this.isStarted = false;
        this.turnLength = 180;
    }

    public boolean isAvailable() {
        if (players.size() < maxPlayers)
            return true;
        else
            return false;
    }

    public boolean isStarted() {
        return isStarted;
    }

    public boolean hasTwoPlayers() {
        if (players.size() == 2)
            return true;
        else
            return false;
    }

    @Override
    public String toString() {
        return "[Room] " + roomId + " [Map] " + map + " [Player] "
                + players.size() + "/" + maxPlayers;
    }

    public Integer getRoomId() {
        return roomId;
    }

    public String getMap() {
        return map;
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }

    public List<UUID> getPlayers() {
        return players;
    }

    /**
     * It starts the game starting from this class state; it creates the
     * PlayersQueue, the GameState and dispose the players in their initial
     * position on the map. Then notify all players that the game is started It
     * also adds all the players into the ClientPlayerTable of the ServerTable
     * class.
     */
    public void startGame() {
        isStarted = true;
        NickName[] nick = NickName.values();
        PlayersQueue playersQueue = createPlayersQueue();
        GameState gameState = new GameState(createMap(), playersQueue,
                turnLength);
        Integer gameKey = serverTable.getGameList().addGame(gameState);
        int i = 0;
        for (Player player : playersQueue.getPlayers()) {
            serverTable.getClientPlace().put(player.getClientId(), gameState);
            serverTable.getClientPlayerTable()
                    .put(player.getClientId(), player);
            player.setInitialPosition(gameState.getGameMap());
            player.setNickName(nick[i].name());
            i++;
        }
        topic.publish("[Game] " + gameKey + " [Map] " + map + " [Players] "
                + playersQueue.getPlayers().size());
        sendInitialMessages();
        topic.publish("now is " + gameState.getCurrentPlayer().toString()
                + "'s turn");
    }

    /**
     * Tells to all the players if they are human or alien and which is their
     * nicknames;
     */
    public void sendInitialMessages() {
        for (UUID id : players) {
            String msg1 = "Your nickname is "
                    + serverTable.getClientPlayerTable().get(id).toString();
            String msg2 = "You are "
                    + serverTable.getClientPlayerTable().get(id).getRole();
            topic.sendTo(id, msg1);
            topic.sendTo(id, msg2);
        }
    }

    public Topic getTopic() {
        return topic;
    }

    private GameMap createMap() {
        MapParser mapParser = new MapParser();
        try {
            mapParser.createMap(map);
        } catch (IOException | SAXException | ParserConfigurationException e) {
            logger.log(Level.SEVERE, "Map Creation problem", e);
        }
        return mapParser.getMap();
    }

    /**
     * It creates the playersQueue of the GameState. If the number of player is
     * even then the number of Aliens and Humans created is the same, otherwise
     * it creates one more alien
     * 
     * @return The playersQueue ready and for the GameState of the match; it's
     *         already shuffled; *
     */
    private PlayersQueue createPlayersQueue() {
        PlayersQueue playersQueue = new PlayersQueue(topic);
        Collections.shuffle(this.players);
        int i;
        for (i = players.size(); i > players.size() / 2; i--) {
            playersQueue.addPlayer(new Alien(players.get(i - 1)));
        }
        for (; i > 0; i--) {
            playersQueue.addPlayer(new Human(players.get(i - 1)));
        }
        playersQueue.shufflePlayers();
        return playersQueue;
    }

}
