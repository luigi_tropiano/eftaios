package it.polimi.ingsw.cg_6.model.item;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import it.polimi.ingsw.cg_6.server.Topic;
import it.polimi.ingsw.cg_6.server.model.GameState;
import it.polimi.ingsw.cg_6.server.model.cards.Card;
import it.polimi.ingsw.cg_6.server.model.cards.item.Adrenaline;
import it.polimi.ingsw.cg_6.server.model.cards.item.ItemCard;
import it.polimi.ingsw.cg_6.server.model.cards.item.Teleport;
import it.polimi.ingsw.cg_6.server.model.character.Human;
import it.polimi.ingsw.cg_6.server.model.character.PlayersQueue;
import it.polimi.ingsw.cg_6.server.model.map.GameMap;
import it.polimi.ingsw.cg_6.server.model.map.MapParser;

import java.io.IOException;
import java.util.UUID;

import javax.xml.parsers.ParserConfigurationException;

import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

public class ItemTest {
    
    private Human human;
    private ItemCard teleport;
    private GameState gameState;
    private GameMap gameMap;
    private PlayersQueue playersQueue;
    
    @Before
    public void init() throws IOException, SAXException, ParserConfigurationException {
        human = new Human(UUID.randomUUID());
        teleport = new Teleport();
        human.addItem(teleport);
        human.addItem(new Adrenaline());
        MapParser mp = new MapParser();
        mp.createMap("Galilei");
        gameMap = mp.getMap();
        playersQueue = new PlayersQueue(new Topic());
        playersQueue.addPlayer(human);
        gameState = new GameState(gameMap, playersQueue, 180);
    }
    
    @Test
    public void humanTest() {
        assertTrue(human.getItems().get(0) instanceof Teleport);
    }
    
    @Test
    public void toStringItemTest() {
        assertEquals("teleport", human.getItems().get(0).toString());
        for (Card item : human.getItems()) {
            System.out.println(item);
        }
    }
    
    @Test
    public void itemDraw() {
        human.addItem(gameState.getItemDeck().drawCard());
        assertTrue(human.getItems().get(1) instanceof ItemCard);
    }
}
