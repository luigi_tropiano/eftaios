package it.polimi.ingsw.cg_6.model.character;

import static org.junit.Assert.assertEquals;
import it.polimi.ingsw.cg_6.server.model.character.Alien;
import it.polimi.ingsw.cg_6.server.model.character.Human;
import it.polimi.ingsw.cg_6.server.model.character.Player;
import it.polimi.ingsw.cg_6.server.model.map.Sector;
import it.polimi.ingsw.cg_6.server.model.map.SecureSector;

import java.awt.Point;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;

public class PlayerTest {
    Player playerA, playerH;
    Sector sector;
    Point point;
    UUID ida = UUID.randomUUID();
    UUID idh = UUID.randomUUID();

    @Before
    public void init() {
        playerA = new Alien(ida);
        playerH = new Human(idh);
        sector = new SecureSector(point = new Point(2, 5));

    }

    @Test
    public void testSetPosition() {
        playerA.setPosition(sector);
        assertEquals(2, sector.getPoint().x);
    }

    @Test
    public void testGetPosition() {
        assertEquals(5, sector.getPoint().y);
    }
}
