package it.polimi.ingsw.cg_6.client.GUI;

import it.polimi.ingsw.cg_6.client.InputConverter;
import it.polimi.ingsw.cg_6.client.rmi.RMIClient;
import it.polimi.ingsw.cg_6.server.model.cards.sector.NoiseEverywhere;

import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JTextArea;

class RMIBuffer implements Runnable {
    private RMIClient client;
    private JTextArea chatBox;
    private RMIGUI rMIGUI;
    public static final Logger logger = Logger.getLogger("escape");
    private int sectortype;
    private int x;
    private int y;
    private boolean notTheFirstTime;

    public RMIBuffer(RMIClient client, JTextArea chatBox, RMIGUI rMIGUI) {
        this.client = client;
        this.chatBox = chatBox;
        this.rMIGUI = rMIGUI;
    }

    @Override
    public void run() {
        Queue<String> buffer = client.getBuffer();
        while (true) {
            String line = buffer.poll();
            if (line != null) {
                chatBox.append("\n" + line);
                if (line.contains(NoiseEverywhere.CHOOSENOISE)) {
                    rMIGUI.setNoise(true);
                }
                if (line.contains(GUI.YOUMOVEDINTO)) {
                    if (notTheFirstTime)
                        rMIGUI.board[x][y] = sectortype;
                    String[] split = line.split(" ");
                    sectortype = rMIGUI.board[InputConverter
                            .charToInt((split[split.length - 2]).charAt(0)) - 1][Integer
                            .parseInt(split[split.length - 1]) - 1];
                    x = InputConverter.charToInt((split[split.length - 2])
                            .charAt(0)) - 1;
                    y = Integer.parseInt(split[split.length - 1]) - 1;
                    rMIGUI.board[InputConverter
                            .charToInt((split[split.length - 2]).charAt(0)) - 1][Integer
                            .parseInt(split[split.length - 1]) - 1] = sectortype + 1;
                    rMIGUI.getPanel().repaint();
                    notTheFirstTime = true;
                }
            } else {
                try {
                    synchronized (buffer) {
                        buffer.wait();
                    }
                } catch (InterruptedException e) {
                    logger.log(Level.SEVERE, "buffer problem", e);
                }
            }
        }
    }
}