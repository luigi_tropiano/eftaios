package it.polimi.ingsw.cg_6.server.model.map;

import java.awt.Point;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class MapParser extends DefaultHandler {

    private int x;
    private int y;
    private String temp;
    private Logger logger = Logger.getLogger("escape");
    private static GameMap map = new GameMap();
    private static Sector alien;
    private static Sector human;
    private Sector sector;
    private Point point;

    public void createMap(String string) throws IOException, SAXException,
            ParserConfigurationException {

        // Create a "parser factory" for creating SAX parsers
        SAXParserFactory spfac = SAXParserFactory.newInstance();

        // Now use the parser factory to create a SAXParser object
        SAXParser sp = spfac.newSAXParser();

        // Create an instance of this class; it defines all the handler methods
        MapParser handler = new MapParser();

        // Finally, tell the parser to parse the input and notify the handler
        String filePath = "res" + File.separatorChar + string + ".xml";
        sp.parse(filePath, handler);

        // lancia per ogni settore la buildNearSector

        for (Sector value : map.getSectors().values()) {
            map.buildNearSectors(value);
        }

        try {
            // costruisci alienSector e humanSector
            map.addSector(alien.getPoint(), alien);
            map.addSector(human.getPoint(), human);
            map.setAlienSector(alien);
            map.setHumanSector(human);
            // lancia la buildNearSector su alienSector e humanSector
            map.buildNearSectors(alien);
            map.buildNearSectors(human);
        } catch (NullPointerException nullPointer) {
            logger.log(Level.SEVERE, "No Alien or Human sector found in "
                    + string + ".xml", nullPointer);
        }
    }

    public GameMap getMap() {
        return map;
    }

    /*
     * Every time the parser encounters the beginning of a new element, it calls
     * this method, which resets the string buffer
     */
    @Override
    public void startElement(String uri, String localName, String qName,
            Attributes attributes) throws SAXException {
        temp = "";
    }

    /*
     * When the parser encounters plain text (not XML elements), it calls(this
     * method, which accumulates them in a string buffer
     */
    @Override
    public void characters(char[] buffer, int start, int length) {
        temp = new String(buffer, start, length);
    }

    /*
     * When the parser encounters the end of an element, it calls this method
     */
    @Override
    public void endElement(String uri, String localName, String qName)
            throws SAXException {
        if ("x".equalsIgnoreCase(qName)) {
            x = (Integer.parseInt(temp));
        } else if ("y".equalsIgnoreCase(qName)) {
            y = (Integer.parseInt(temp));
        } else if ("dangerous".equalsIgnoreCase(qName)) {
            point = new Point(x, y);
            sector = new DangerousSector(point);
            map.addSector(point, sector);
        } else if ("secure".equalsIgnoreCase(qName)) {
            point = new Point(x, y);
            sector = new SecureSector(point);
            map.addSector(point, sector);
        } else if ("escape".equalsIgnoreCase(qName)) {
            point = new Point(x, y);
            sector = new EscapeHatchSector(point);
            map.addSector(point, sector);
        } else if ("alien".equalsIgnoreCase(qName)) {
            point = new Point(x, y);
            alien = new AlienSector(point);
        } else if ("human".equalsIgnoreCase(qName)) {
            point = new Point(x, y);
            human = new HumanSector(point);
        }
    }
}
