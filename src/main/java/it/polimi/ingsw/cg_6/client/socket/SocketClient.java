package it.polimi.ingsw.cg_6.client.socket;

import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;
import java.util.UUID;

/**
 * The socket client
 * 
 * @author Daniele Parigi
 * @author Luigi Tropano
 */
public class SocketClient implements Runnable {
    private String ip;
    private int port;
    private UUID id;
    private SocketCommunicator communicator;
    private SubscriberThread subscriberThread;

    public SocketClient(String ip, int port) {
        this.ip = ip;
        this.port = port;
        this.id = UUID.randomUUID();
    }

    public UUID getId() {
        return id;
    }

    /**
     * start a scanner in for the input request a join to access room and Topic
     * and start to scanner the command from the user
     */
    @Override
    public void run() {
        String response = "";
        String command = "";
        try {
            Scanner stdin = new Scanner(System.in);
            Socket socket = new Socket(ip, port);
            communicator = new SocketCommunicator(socket);
            System.out.println("Connection established");
            command = id + " join";
            // join the room
            communicator.send(command);
            response = communicator.receive();
            System.out.println(response);
            //subscribe to topic
            subscriberThread = new SubscriberThread(ip, id);
            subscriberThread.start();
            communicator.close();
            
            //start the scanner for user input
            do {
                command = id + " " + stdin.nextLine();
                if (command.equals(id + " exit"))
                    break;
                socket = new Socket(ip, port);
                // open the socket
                communicator = new SocketCommunicator(socket);
                // pars the line before sending it to server
                command = new OutLineParser(command).parser();
                if (command != null) {
                    // send the command
                    communicator.send(command);
                    //receive the responses
                    response = communicator.receive();
                    // write the responses on screen
                    System.out.println(response);
                    // close communicator and socket
                    communicator.close();
                }
            } while (true);
            subscriberThread.interrupt();
            stdin.close();
        } catch (IOException ex) {
            throw new AssertionError(
                    "Weird errors with I/O occured, please verify environment config",
                    ex);
        }

    }

    public SocketCommunicator getCommunicator() {
        return communicator;
    }
}