package it.polimi.ingsw.cg_6.client.socket;

public interface Communicator {

    void send(String msg);

    String receive();

    void close();

}
