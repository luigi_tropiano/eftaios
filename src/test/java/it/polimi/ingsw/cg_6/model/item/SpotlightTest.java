package it.polimi.ingsw.cg_6.model.item;

import it.polimi.ingsw.cg_6.server.Topic;
import it.polimi.ingsw.cg_6.server.controller.Controller;
import it.polimi.ingsw.cg_6.server.controller.ServerTable;
import it.polimi.ingsw.cg_6.server.model.GameState;
import it.polimi.ingsw.cg_6.server.model.cards.item.Spotlight;
import it.polimi.ingsw.cg_6.server.model.character.Alien;
import it.polimi.ingsw.cg_6.server.model.character.Human;
import it.polimi.ingsw.cg_6.server.model.character.Player;
import it.polimi.ingsw.cg_6.server.model.character.PlayersQueue;
import it.polimi.ingsw.cg_6.server.model.map.GameMap;
import it.polimi.ingsw.cg_6.server.model.map.MapParser;

import java.awt.Point;
import java.io.IOException;
import java.util.UUID;

import javax.xml.parsers.ParserConfigurationException;

import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

public class SpotlightTest {

    private Player player1, player2, player3, player4;
    private Topic topic;
    private GameState gameState;
    private MapParser mp;
    private GameMap gameMap;
    private PlayersQueue playersQueue;
    private int turnLength;
    private Controller controller;
    private ServerTable serverTable;

    @Before
    public void init() throws IOException, SAXException,
            ParserConfigurationException {
        turnLength = 180;
        player1 = new Alien(UUID.randomUUID());
        player2 = new Alien(UUID.randomUUID());
        player3 = new Human(UUID.randomUUID());
        player4 = new Human(UUID.randomUUID());
        mp = new MapParser();
        mp.createMap("Galilei");
        gameMap = mp.getMap();
        topic = new Topic();
        playersQueue = new PlayersQueue(topic);

        playersQueue.addPlayer(player1);
        playersQueue.addPlayer(player2);
        playersQueue.addPlayer(player3);
        playersQueue.addPlayer(player4);
        gameState = new GameState(gameMap, playersQueue, turnLength);

        serverTable = new ServerTable();
        serverTable.getClientPlace().put(player1.getClientId(), gameState);
        serverTable.getClientPlace().put(player2.getClientId(), gameState);
        serverTable.getClientPlace().put(player3.getClientId(), gameState);
        serverTable.getClientPlace().put(player4.getClientId(), gameState);
        serverTable.getClientPlayerTable().put(player1.getClientId(), player1);
        serverTable.getClientPlayerTable().put(player2.getClientId(), player2);
        serverTable.getClientPlayerTable().put(player3.getClientId(), player3);
        serverTable.getClientPlayerTable().put(player4.getClientId(), player4);
        controller = new Controller(serverTable);
    }

    @Test
    public void rightActivateTest(){
       gameState.setCurrentPlayer(player4);
       player1.setPosition(gameMap.getSector(new Point(4, 8))); 
       player2.setPosition(gameMap.getSector(new Point(4, 9))); 
       player3.setPosition(gameMap.getSector(new Point(6, 8))); 
       player4.setPosition(gameMap.getSector(new Point(5, 9))); 
       player4.addItem(new Spotlight());
       System.out.println(controller.use(player4.getClientId(), player4.getClientId() + " use spotlight 5 9"));
    }

}
