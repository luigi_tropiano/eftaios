package it.polimi.ingsw.cg_6.server.model.character;

import it.polimi.ingsw.cg_6.client.InputConverter;
import it.polimi.ingsw.cg_6.server.model.GameState;
import it.polimi.ingsw.cg_6.server.model.cards.Card;
import it.polimi.ingsw.cg_6.server.model.decks.ItemDeck;
import it.polimi.ingsw.cg_6.server.model.map.GameMap;
import it.polimi.ingsw.cg_6.server.model.map.Sector;
import it.polimi.ingsw.cg_6.server.model.move.Move;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * The playing player
 * 
 * @author Luigi Tropiano
 * @author Daniele Parigi
 *
 */
public abstract class Player {

    private UUID clientId;
    private String nickName;
    private Sector position;
    private List<Card> items;
    private List<String> log;
    private boolean didAttack;
    private boolean didMove;
    private boolean didDraw;
    private Move move;
    private boolean haveToNoise;
    private boolean canAttack;
    private boolean isWinner;

    public Player(UUID clientId) {
        items = new ArrayList<Card>();
        log = new ArrayList<String>();
        this.clientId = clientId;
    }

    public UUID getClientId() {
        return clientId;
    }

    public String setPosition(Sector position) {
        this.position = position;
        return "you moved into sector "
                + InputConverter.intToChar(position.getPoint().x) + " "
                + position.getPoint().y;
    }

    public Sector getPosition() {
        return position;
    }

    public void addItem(Card card) {
        items.add(card);
    }

    public List<Card> getItems() {
        return items;
    }

    public List<String> getLog() {
        return log;
    }

    public String getLastLog() {
        return log.get(log.size() - 1);
    }

    public boolean getDidAttack() {
        return didAttack;
    }

    public void setDidAttack(boolean didAttack) {
        this.didAttack = didAttack;
    }

    public boolean getDidMove() {
        return didMove;
    }

    public void setDidMove(boolean didMove) {
        this.didMove = didMove;
    }

    public boolean getDidDraw() {
        return didDraw;
    }

    public void setDidDraw(boolean didDraw) {
        this.didDraw = didDraw;
    }

    public Move getMove() {
        return move;
    }

    public void setMove(Move move) {
        this.move = move;
    }

    public abstract void setInitialPosition(GameMap gameMap);

    public boolean haveDefense(ItemDeck itemDeck) {
        for (Card item : items)
            if (item.toString() == "defense") {
                itemDeck.discardCard(item);
                items.remove(item);
                return true;
            }
        return false;
    }

    public boolean getHaveToNoise() {
        return haveToNoise;
    }

    public void setHaveToNoise(boolean haveToNoise) {
        this.haveToNoise = haveToNoise;
    }

    /**
     * It removes this player from the playing players array and add him to the
     * removed players one; then it adds to log this event.
     * 
     * @param gameState
     *            the current GameState of the match
     */
    public void kill(GameState gameState) {
        gameState.getPlayerQueue().removePlayer(this);
        gameState.addLog(gameState.getCurrentPlayer().toString()
                + " kills the " + this.getRole() + " " + this.toString());

        if (!gameState.isThereAnyHuman()) {
            for (Player player : gameState.getPlayerQueue().getPlayers())
                player.setWinner(true);
            gameState.endGame();
        }
    }

    public abstract String getRole();

    /**
     * Set all players attribute to false
     * 
     */
    public void resetFlags() {
        this.didAttack = false;
        this.didDraw = false;
        this.didMove = false;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    @Override
    public String toString() {
        return "[" + nickName + "]";
    }

    public boolean getCanAttack() {
        return canAttack;
    }

    public void setCanAttack(boolean canAttack) {
        this.canAttack = canAttack;
    }

    public boolean isWinner() {
        return isWinner;
    }

    public void setWinner(boolean isWinner) {
        this.isWinner = isWinner;
    }

}