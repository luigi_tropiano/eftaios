package it.polimi.ingsw.cg_6.server;

import it.polimi.ingsw.cg_6.client.socket.SocketCommunicator;
import it.polimi.ingsw.cg_6.server.controller.Controller;
import it.polimi.ingsw.cg_6.server.controller.ServerTable;
import it.polimi.ingsw.cg_6.server.rmi.ActionInterface;
import it.polimi.ingsw.cg_6.server.rmi.BrokerInterface;
import it.polimi.ingsw.cg_6.server.socket.Broker;
import it.polimi.ingsw.cg_6.server.socket.ClientHandler;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Server {

    private int port;
    private ServerSocket serverSocket;
    private ServerTable serverTable;
    private static Logger logger = Logger.getLogger("escape");

    public Server(int port) throws RemoteException {
        this.port = port;
        serverTable = new ServerTable();
    }

    private void startRMI() throws RemoteException, AlreadyBoundException {
        System.out.println("Constructing server implementation");
        ActionInterface actions = new Controller(serverTable);
        ActionInterface stub = (ActionInterface) UnicastRemoteObject
                .exportObject(actions, 0);
        System.out.println("Binding server implementation to registry...");
        Registry registry = LocateRegistry.createRegistry(1099);
        registry.bind("server", stub);
        System.out.println("Waiting for invocations from clients...");
    }

    private void startRMIBroker() throws RemoteException, AlreadyBoundException{
        System.out.println("Starting the RMI Broker Service"); 
        BrokerInterface broker = serverTable;
        BrokerInterface stub = (BrokerInterface) UnicastRemoteObject.exportObject(broker, 0);
        Registry brokerRegistry = LocateRegistry.createRegistry(1098);
        brokerRegistry.bind("broker", stub);
    }

    public void startServer() {
        startBroker();
        try {
            /*
             * ExecutorService represents an asynchronous execution mechanism
             * which is capable of executing tasks in the background.
             * Executors.newCachedThreadPool() Creates a thread pool that
             * creates new threads as needed, but will reuse previously
             * constructed threads when they are available.
             */
            ExecutorService executor = Executors.newCachedThreadPool();
            serverSocket = new ServerSocket(port);
            System.out.println("Server ready");
            while (isStopped()) {
                Socket socket = serverSocket.accept();
                // Submits a Runnable task for execution
                executor.submit(new ClientHandler(
                        new SocketCommunicator(socket), serverTable));
            }
            executor.shutdown();
            serverSocket.close();
        } catch (IOException ex) {
            throw new AssertionError(
                    "Weird errors with I/O occured, please verify environment config",
                    ex);
        }
    }

    public void startBroker() {
        System.out.println("Starting the Broker Service");
        Broker broker = new Broker(serverTable);
        broker.start();
    }

    private boolean isStopped() {
        return true;
    }

    public static void main(String[] args) {
        Server server;
        try {
            server = new Server(29999);
            server.startRMI();
            server.startRMIBroker();
            server.startServer();
        } catch (RemoteException | AlreadyBoundException e) {
            logger.log(Level.WARNING, "server", e);
        }
    }
}