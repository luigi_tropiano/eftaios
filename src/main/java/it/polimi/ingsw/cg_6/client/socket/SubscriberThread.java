package it.polimi.ingsw.cg_6.client.socket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SubscriberThread extends Thread {
    private Socket subSocket;
    private String address;
    private final int port = 28999;
    private UUID id;
    private PrintWriter out;
    protected BufferedReader in;
    protected Logger logger = Logger.getLogger("escape");

    /**
     * As soon as the thread gets instanziated, you subscribe to the Broker.
     * 
     * @param id
     *            the id related to the client.
     */
    public SubscriberThread(String IP, UUID id) {
        this.address = IP;
        this.id = id;
        try {
            subscribe();
        } catch (IOException e) {
            logger.log(Level.SEVERE, "subscribe", e);
        }
    }

    /**
     * After subscribing, this method keep looking for messages from the
     * publisher.
     */
    @Override
    public void run() {
        while (true) {
            receive();
            try {
                // it wait 5 ms to reduce clock cycles
                // especially if publisher crashes
                Thread.sleep(5);
            } catch (InterruptedException e) {
                logger.log(Level.SEVERE, "sleep", e);
            }

        }
    }

    /**
     * Method that receives possible messages from the publisher
     * 
     */
    protected void receive() {
        String msg = null;
        try {
            msg = in.readLine();
            if (msg != null) {
                System.out.println(msg);
            }
        } catch (IOException e) {
            logger.log(Level.SEVERE, "receive", e);
        }
    }

    /**
     * It connects to the broker creating a socket and open a steam for incoming
     * messages from the publisher
     * 
     * @throws UnknownHostException
     * @throws IOException
     */
    private void subscribe() throws UnknownHostException, IOException {
        subSocket = new Socket(address, port);
        in = new BufferedReader(new InputStreamReader(
                subSocket.getInputStream()));
        out = new PrintWriter(subSocket.getOutputStream());
        out.println(id);
        out.flush();
        // out.close();
    }
}