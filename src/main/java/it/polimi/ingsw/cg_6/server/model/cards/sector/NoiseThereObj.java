package it.polimi.ingsw.cg_6.server.model.cards.sector;

import it.polimi.ingsw.cg_6.client.InputConverter;
import it.polimi.ingsw.cg_6.server.model.GameState;
import it.polimi.ingsw.cg_6.server.model.cards.item.ItemCard;
import it.polimi.ingsw.cg_6.server.model.character.Player;

/**
 * the noise there card with item attacked
 * 
 * @author Daniele Parigi
 * @author Luigi Tropiano
 */
public class NoiseThereObj extends SectorCard {
    /**
     * activate of the noise everywhere card, draw an item and log the result
     * 
     * @param player
     * @param gameState
     * @return the result of draw
     */
    @Override
    public String activate(Player player, GameState gameState) {
        ItemCard item = (ItemCard) gameState.getItemDeck().drawCard();
        player.addItem(item);
        gameState.addLog(player.toString() + " draws a sector card");
        gameState.addLog(player.toString() + " finds an item");
        gameState.addLog(player.toString() + " makes noise in sector "
                + InputConverter.intToChar(player.getPosition().getPoint().x)
                + " " + player.getPosition().getPoint().y);
        return "you found [" + item.toString() + "]. you made noise in sector "
                + InputConverter.intToChar(player.getPosition().getPoint().x)
                + " " + player.getPosition().getPoint().y;
    }
}
