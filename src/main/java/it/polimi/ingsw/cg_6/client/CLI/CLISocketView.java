package it.polimi.ingsw.cg_6.client.CLI;

import it.polimi.ingsw.cg_6.client.socket.SocketClient;


public class CLISocketView extends CLI{    
    private CLISocketView() {
    }

    /**
     * Runs the SocketClient
     */
    public static void start() {

        String HOST = waitForIP();
        int PORT = 29999;

        // creates a new SocketClient
        SocketClient SocketClient = new SocketClient(HOST, PORT);
        // runs the SocketClient
        new Thread(SocketClient).start();
    }
}